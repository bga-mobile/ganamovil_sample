function encrypt(inputStr) {
	var gPassphrase  = AppConfig.PASSPRHASE;
	var gBase64EncryptedString = "";
	var myEncryptedText = "";

	if (kony.os.deviceInfo().name == "android") {
       var gAESKey = AESCrypto.generateAESKey(gPassphrase, "AES"); 
       gBase64EncryptedString = AESCrypto.encrypt(inputStr, gAESKey); 
	} else 
		if (kony.os.deviceInfo().name == "iPhone" || kony.os.deviceInfo().name == "iPad") { 
			gBase64EncryptedString = AESCrypto.encryptData(inputStr, gPassphrase);
        }  

	return gBase64EncryptedString;
}

function decrypt(inputStrEncrypt) {
	var gPassphrase  = AppConfig.PASSPRHASE;
	var decryptedString = "";
	
	if (kony.os.deviceInfo().name == "android") {
		var gAESKey = AESCrypto.generateAESKey(gPassphrase, "AES");
        decryptedString = AESCrypto.decrypt(inputStrEncrypt, gAESKey);
    } else 
    	if (kony.os.deviceInfo().name == "iPhone" || kony.os.deviceInfo().name == "iPad") {
			decryptedString = AESCrypto.decryptData(inputStrEncrypt, gPassphrase);
        } 

	return decryptedString;
}