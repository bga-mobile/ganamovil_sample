kony.print('MVP.PresenterMapaResumen >>> Loading JS file mapaPresenterResumen.js');

var PresenterMapaResumen = (function (kony, undefined) {

  var PresenterMapaResumen = kony.Stapes.subclass({

    'constructor': function (pViewMapaResumen, pModelPunto, pPopPregunta, pPopMensaje) {
      this.viewMapaResumen = pViewMapaResumen;

      this.modelPunto = pModelPunto;
      this.modelUtils = new ModelUtils();

	  	this.popViewPregunta = new PopViewPregunta(pPopPregunta);
      this.popViewMensaje = new PopViewMensaje(pPopMensaje);

      this.init();
    },

    'init': function() {
      this.popViewPregunta.setTituloAndMensaje(AppConfig.APP_NAME,	MensajePop.CERRAR_SESION);

      this.watchViewMapaResumen();
      this.watchPopViewPregunta();
      this.watchPopViewMensaje();
      this.watchModelPunto();
      this.watchModelUtils();
    },

    'setModelPunto': function(pModelPunto) {
      this.modelPunto = pModelPunto;
      this.modelUtils.cancelService(this.modelPunto.handleWSGetPuntoPropiedades);
    },

    'watchViewMapaResumen': function () {
			this.viewMapaResumen.on({

        'exitAplication': function() {
	      	kony.print('MVP.PresenterMapaResumen >>> Ask PreguntaView to show');
          this.popViewPregunta.setTituloAndMensaje(
              AppConfig.APP_NAME,
              MensajePop.CERRAR_SESION,
              kPregunta.EXIT_APP
          );
	      	this.popViewPregunta.show();
	      },

	      'backAplicaction': function() {
	      	kony.print('MVP.PresenterMapaResumen >>> Ask UtilsModel to backApplication');
	      	this.modelUtils.backApplication(kFrmMobile.MAPA);
	      },

	      'wsPuntosPropiedades': function() {
          kony.print('MVP.PresenterMapaResumen >>> Ask modelPunto to wsGetPuntoPropiedades');
	      	this.modelPunto.wsGetPuntoPropiedades();
	      }

			}, this);
    },

    'watchPopViewMensaje': function() {
      this.popViewMensaje.on({
        'ok': function() {
          kony.print('MVP.PresenterMapaResumen >>> Ask MensajeView to dismiss');
          this.popViewMensaje.dismiss();
        }
      }, this);
    },

		/**
	   * Respond to events emitted by the popView Pregunta
	   */
	  'watchPopViewPregunta': function () {
      /**
       * event's handler's
       */
      this.popViewPregunta.on({

	      'yesExitApp': function() {
	      	kony.print('MVP.PresenterMapaResumen >>> Ask UtilModel to exitApplication');
	      	this.modelUtils.exitApplication();
	      },

	      'no': function() {
	      	kony.print('MVP.PresenterMapaResumen >>> Ask PreguntaView to dismiss');
	      	this.popViewPregunta.dismiss();
	      }

	    }, this);

	  },

    'watchModelPunto': function() {
      this.modelPunto.on({
        'changedWsGetPuntoPropiedades': function(info) {
          if (info.code === 0) {
            //this.viewMapaResumen.loadPuntoPropiedades('', false);
            kony.print('MVP.PresenterMapaResumen >>> Ask viewMapaResumen to loadPuntoResumen');
            this.viewMapaResumen.loadPuntoResumen(info.punto, true);
          } else {
            this.viewMapaResumen.loadPuntoPropiedades('Error al actualizar las propiedades...', true);
            this.popViewMensaje.setTituloAndMensaje(
              info.message.titulo,
              info.message.mensaje
            );
            this.popViewMensaje.show();
          }
        }
      }, this);
    },

    /**
     * Respond to events emitted by the mmodel utils
     */
    'watchModelUtils': function() {
      /**
       * event's handler's
       */
      this.modelUtils.on({
        'changedExitAplication' : function() {
          kony.print('MVP.PresenterMapaResumen >>> Ask PregutaView to dismiss >>> Ask MapaView to hide');
          this.modelUtils.cancelService(this.modelPunto.handleWSGetPuntoPropiedades);
          this.popViewPregunta.dismiss();
          //this.viewMapa.destroy();
        },

        'changedBackAplication': function() {
          kony.print('MVP.PresenterMapaResumen >>> Ask MapaView to hide');
          this.modelUtils.cancelService(this.modelPunto.handleWSGetPuntoPropiedades);
          this.emit('changedBackMapaResumen');
          //this.viewMapa.destroy();
        }
      }, this);

    }

  });

  return PresenterMapaResumen;

})(kony);
