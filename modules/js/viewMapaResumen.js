kony.print('MVP.ViewMapaResumen >>> Loading JS file viewMapaResumen.js');

var ViewMapaResumen = (function (kony, undefined) {

  function hbxImgBackOnClick() {
	  this.emit('backAplicaction');
  }

  function btnExitOnClick() {
		this.emit('exitAplication');
  }

  /**
   * Class Mapa View
   */
  var ViewMapaResumen = kony.Stapes.subclass({

    'constructor': function() {
      this.init();
    },

    'init': function() {
      frmMapaResumen.segMapaResumen.widgetDataMap = {
        key: 'key',
        lblTitulo: 'clave',
        lblDescripcion: 'valor'
      };

      frmMapaResumen.lblMensajeMapaResumen.isVisible = true;
      frmMapaResumen.segMapaResumen.isVisible = false;
			frmMapaResumen.hbxClientesEspera.isVisible = false;
			frmMapaResumen.hbxTelefono.isVisible = false;

      this.bindUIEventHandlers();
    },

    'bindUIEventHandlers': function() {
      frmMapaResumen.hbxImgBack.onClick = hbxImgBackOnClick.bind(this);
      //#ifdef android
        frmMapaResumen.onDeviceBack = hbxImgBackOnClick.bind(this);
      //#endif
      frmMapaResumen.btnExit.onClick = btnExitOnClick.bind(this);
    },

    'show': function() {
      this.emit('wsPuntosPropiedades');
      frmMapaResumen.show();
    },

    'destroy': function() {
	  	frmMapaResumen.destroy();
	  },

		'setPuntoSelected': function(punto) {
			_puntoSelected = punto;
		},

		'loadPuntoPropiedades': function(text, isShow) {
			frmMapaResumen.lblMensajeMapaResumen.text = text;
			frmMapaResumen.lblMensajeMapaResumen.isVisible = isShow;
      frmMapaResumen.segMapaResumen.isVisible = !isShow;
		},

    'loadHeaderPuntoResumen': function(punto) {
      frmMapaResumen.lblMapaResumen.text = punto.descDpto;
      frmMapaResumen.lblMapaResumenDetalle.text = punto.nombre;
      frmMapaResumen.imgLeftMapaResumen.src = punto.idTipo == 'A'? 'btn_agencia.png' : 'btn_atm.png';
    },

    'loadPuntoResumen': function(punto, isShow) {
    	frmMapaResumen.segMapaResumen.setData(punto.propiedades.values());

    	frmMapaResumen.phnTelefono.text = punto.telefono;
    	if (punto.telefono == 'S/N')
    		frmMapaResumen.phnTelefono.setEnabled(false);
    	else
    		frmMapaResumen.phnTelefono.setEnabled(true);

      frmMapaResumen.lblMensajeMapaResumen.isVisible = !isShow;
      frmMapaResumen.segMapaResumen.isVisible = isShow;
      frmMapaResumen.hbxClientesEspera.isVisible = false;
      frmMapaResumen.hbxTelefono.isVisible = true;
    }

  });

  return ViewMapaResumen;

})(kony);
