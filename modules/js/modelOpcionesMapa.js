kony.print('MVP.ModelOpcionesMapa >>> Loading JS file modelOpcionesMapa.js');

var ModelOpcionesMapa = (function(kony,undefined) {
  var _menuPuntosAtencion;
  var _menuFiltroBusqueda;
  var _info;

  function getImagenIcon(tipoPunto) {
    var img;
    switch (tipoPunto) {
      case 'A':
        img = 'btn_agencia.png';
      break;
      case 'T':
        img = 'btn3_atm.png';
      break;
      default:
        img = 'btn_sucursal.png';
      break;
    }
    return img;
  }

  function getImagenRadio(isSelected) {
    return isSelected? 'radio_checked.png' : 'radio_unchecked.png';
  }

  function getImagenCheck(isSelected) {
    return isSelected? 'icon_check.png' : 'icon_uncheck.png';
  }

  function getImagenSwitch(isSelected) {
    var dos;
  	//#ifdef android
  		dos = '_2';
  	//#endif
    return isSelected? 'ic_switch_on'+dos+'.png' : 'ic_switch_off'+dos+'.png';
  }

  function putOpcionPuntoAtencion(key, opcion) {
    var value = {
      'key': opcion.key,
      'isSelected': opcion.isSelected,
      'imgIcon': getImagenIcon(opcion.key),
      'descripcion': opcion.descripcion,
      'imgSelected': getImagenRadio(opcion.isSelected)
    };
    _menuPuntosAtencion.put(key, value);
  }

  function putOpcionFiltroBusqueda(key, opcion) {
    var value = {
      'key': opcion.key,
      'isSelected': opcion.isSelected,
      'departamento': opcion.departamento,
      'imgSelected': getImagenRadio(opcion.isSelected)
    };
    _menuFiltroBusqueda.put(key, value);
  }

  var ModelOpcionesMapa = kony.Stapes.subclass({

    'constructor': function() {
      _menuPuntosAtencion = new Hashtable();
      _menuFiltroBusqueda = new Hashtable();

      _info = {};

      this.init();
    },

    'init': function() {
      this.loadMenuPuntosAtencion();
      this.loadMenuFiltroBusqueda();
    },

    'loadMenuPuntosAtencion': function() {
      var opcionPuntoAtencion = {};
      //sucursales & agencia
      opcionPuntoAtencion = {
        'key': 'A',
        'isSelected': false,
        'descripcion': 'Sucursales y Agencias'
      };
      putOpcionPuntoAtencion('A', opcionPuntoAtencion);

      //cajeros automaticos
      opcionPuntoAtencion = {
        'key': 'T',
        'isSelected': false,
        'descripcion': 'Cajeros Automáticos'
      };
      putOpcionPuntoAtencion('T', opcionPuntoAtencion);
    },

    'loadMenuFiltroBusqueda': function() {
      var opcionFiltroBusqueda = {};

      opcionFiltroBusqueda = {
        'key': '6',
        'isSelected': false,
        'departamento': 'Tarija'
      };
      putOpcionFiltroBusqueda(6, opcionFiltroBusqueda);

      opcionFiltroBusqueda = {
        'key': '7',
        'isSelected': false,
        'departamento': 'Santa Cruz'
      };
      putOpcionFiltroBusqueda(7, opcionFiltroBusqueda);

      opcionFiltroBusqueda = {
        'key': '5',
        'isSelected': false,
        'departamento': 'Potosi'
      };
      putOpcionFiltroBusqueda(5, opcionFiltroBusqueda);

      opcionFiltroBusqueda = {
        'key': '9',
        'isSelected': false,
        'departamento': 'Pando'
      };
      putOpcionFiltroBusqueda(9, opcionFiltroBusqueda);

      opcionFiltroBusqueda = {
        'key': '4',
        'isSelected': false,
        'departamento': 'Oruro'
      };
      putOpcionFiltroBusqueda(4, opcionFiltroBusqueda);

      opcionFiltroBusqueda = {
        'key': '2',
        'isSelected': false,
        'departamento': 'La Paz'
      };
      putOpcionFiltroBusqueda(2, opcionFiltroBusqueda);

      opcionFiltroBusqueda = {
        'key': '3',
        'isSelected': false,
        'departamento': 'Cochabamba'
      };
      putOpcionFiltroBusqueda(3, opcionFiltroBusqueda);

      opcionFiltroBusqueda = {
        'key': '1',
        'isSelected': false,
        'departamento': 'Chuquisaca'
      };
      putOpcionFiltroBusqueda(1, opcionFiltroBusqueda);

      opcionFiltroBusqueda = {
        'key': '8',
        'isSelected': false,
        'departamento': 'Beni'
      };
      putOpcionFiltroBusqueda(8, opcionFiltroBusqueda);
    },

    'getMenuPuntoAtencion': function() {
      _info.opcionesPuntosAtencion = _menuPuntosAtencion.values();
      this.emit('changedMenuPuntosAtencion', _info);
    },

    'getMenuFiltroBusqueda': function() {
      _info.opcionesFiltroBusqueda = _menuFiltroBusqueda.values();
      this.emit('changedMenuFiltroBusqueda', _info);
    },

    'findTipoPuntoAtencion': function(key) {
      return _menuPuntosAtencion.get(key);
    },

    'findFiltroBusqueda': function(key) {
      return _menuFiltroBusqueda.get(key);
    }

  });

  return ModelOpcionesMapa;
})(kony);
