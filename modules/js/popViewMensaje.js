kony.print('MVP.PopViewMensaje >>> Loading JS file popViewMensaje.js');

/**
 * Class Pregunta View
 */
var PopViewMensaje = (function(kony, undefined) {

	var popMensaje;

	function btnOkOnClick() {
		this.emit('ok');
	}

	var PopViewMensaje = kony.Stapes.subclass({

		'constructor': function (pPopMensaje) {
			popMensaje = pPopMensaje;

			this.bindUIEventHandlers();
		},

		'init': function () {
			popMensaje.lblTitulo.text = this.titulo;
			popMensaje.lblMensaje.text = this.mensaje;
		},

	  /**
	  * Event Handlers for binding this view
	  */
	  'bindUIEventHandlers' : function () {
	  	popMensaje.btnOk.onClick = btnOkOnClick.bind(this);
	  	//#ifdef android
	  		popMensaje.onDeviceBack = btnOkOnClick.bind(this);
	  	//#endif
	  },

	  'show': function () {
			this.init();
	  	popMensaje.show();
	  },

	  'dismiss': function () {
	  	popMensaje.dismiss();
	  },

		'setTituloAndMensaje': function (pTitulo, pMensaje) {
			this.titulo = pTitulo;
			this.mensaje = pMensaje;
		}

	});

	return PopViewMensaje;

})(kony);
