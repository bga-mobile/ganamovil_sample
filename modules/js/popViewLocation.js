kony.print('MVP.PopViewLocation >>> Loading JS file popViewLocation.js');

var PopViewLocation = (function(kony, undefined) {
  var _context;
  var popToastDataLocation;

  function vbxImgCloseOnClick() {
    this.emit('closePopViewLocation');
  }

  function hbxBodyDataLocationOnClick() {
    this.emit('viewMapaResumen');
  }

  function popToasDataLocationOnHide() {
    this.emit('hidePopViewLocation');
  }

  var PopViewLocation = kony.Stapes.subclass({

    'constructor': function(pPopViewLocation, pContext) {
      popToastDataLocation = pPopViewLocation;
      _context = pContext;

      this.init();
      this.bindUIEventHandlers();
    },

    'init': function() {
      popToastDataLocation.setContext({"widget":_context,"anchor":"bottom","sizetoanchorwidth":true});
    	popToastDataLocation.transparencyBehindThePopup = 100;

    	//popToastDataLocation.extendTop = true;
			//popToastDataLocation.segEspera.screenLevelWidget = false;
			//popToastDataLocation.segEspera.viewType = constants.SEGUI_VIEW_TYPE_TABLEVIEW;
      popToastDataLocation.segEspera.selectionBehavior = constants.SEGUI_DEFAULT_BEHAVIOR;
      popToastDataLocation.segEspera.sectionHeaderTemplate = tempHeaderHbxClientesEspera;
    	popToastDataLocation.segEspera.rowTemplate = tempHbxClientesEspera;
      popToastDataLocation.segEspera.widgetDataMap = {
    		lblCantidadEsperaDesc:"cantidadDesc",
    		lblTiempoEsperaDesc:"tiempoDesc",
    		lblCantidadEspera:"cantidad",
    		lblTiempoEspera:"tiempo"
     	};
    },

    'bindUIEventHandlers': function() {
      popToastDataLocation.vbxImgClose.onClick = vbxImgCloseOnClick.bind(this);
    	popToastDataLocation.hbxBodyDataLocation.onClick = hbxBodyDataLocationOnClick.bind(this);
      popToastDataLocation.hbxClientesEspera.onClick = hbxBodyDataLocationOnClick.bind(this);
    	popToastDataLocation.onHide = popToasDataLocationOnHide.bind(this);
    },

    'show': function () {
      popToastDataLocation.show();
      //kony.print('MVP.PopViewLocation >>> PopViewLocation show >>> ');
    },

    'dismiss': function() {
      popToastDataLocation.dismiss();
      //popToastDataLocation.destroy();
    },

    'loadLocation': function(plocation) {
      kony.print('MVP.PopViewLocation >>> PopViewLocation loadLocation >>> begin >>> '+JSON.stringify(plocation));

      //si es una agencia y no es una agencia desplazada
      if (plocation.idTipo === 'A' && plocation.idSubTipo !== 'D') {
        popToastDataLocation.hbxClientesEspera.isVisible = true;

        popToastDataLocation.lblClientesEspera.text = 'Cargando clientes en espera...';
        popToastDataLocation.lblClientesEspera.isVisible = true;
        popToastDataLocation.segEspera.isVisible = false;

        this.emit('wsGetPuntosTickets', plocation);

				kony.print('MVP.PopViewLocation >>> PopViewLocation loadLocation >>> true>>> ');
      } else {
        popToastDataLocation.hbxClientesEspera.isVisible = false;
        kony.print('MVP.PopViewLocation >>> PopViewLocation loadLocation >>> false>>> ');
      }

			popToastDataLocation.lblNotaClientesEspera.isVisible = false;

      popToastDataLocation.lblDenominacion.text = plocation.nombre;
      popToastDataLocation.lblDireccion.text = plocation.direccion;

      kony.print('MVP.PopViewLocation >>> PopViewLocation loadLocation >>> end >>> ');
    },

    'loadLocationTickets': function(puntoTickets, puntoSelected) {
      kony.print('MVP.PopViewLocation >>> PopViewLocation loadLocationTickets puntoTickets >>> '+JSON.stringify(puntoTickets));
      kony.print('MVP.PopViewLocation >>> PopViewLocation loadLocationTickets puntoSelected >>> '+JSON.stringify(puntoSelected));
      kony.print('MVP.PopViewLocation >>> PopViewLocation loadLocationTickets segEspera >>> '+JSON.stringify(puntoTickets[0].tickets.values()));
      if (puntoTickets[0].isVisible) {
        popToastDataLocation.segEspera.setData(puntoTickets[0].tickets.values());

        if (!puntoSelected.isCentral)
          popToastDataLocation.segEspera.removeAt(1, 0);

        popToastDataLocation.lblClientesEspera.isVisible = false;

        popToastDataLocation.segEspera.isVisible = true;

        popToastDataLocation.lblNotaClientesEspera.text = '*Ejecutivos Comerciales atienden por orden de llegada';
        popToastDataLocation.lblNotaClientesEspera.isVisible = true;
      } else {
        popToastDataLocation.lblClientesEspera.text = 'En este horario puedes consultar el cajero automático más cercano';
        popToastDataLocation.lblClientesEspera.isVisible = true;

        popToastDataLocation.segEspera.isVisible = false;
        popToastDataLocation.lblNotaClientesEspera.isVisible = false;
      }
    },

    'setTextLblClientesEspera': function(txt) {
      popToastDataLocation.lblClientesEspera.text = txt;
    }


  });

  return PopViewLocation;
})(kony);
