kony.print('MVP.ModelUtils >>> Loading JS file modelUtils.js');

/**
 * Class Utils Model
 */

var ModelUtils = (function(kony, undefined) {
	var _info;

	function asyncCallbackFechaProceso(status, results){
		if (status==400) {
			kony.print('MVP.ModelPunto >>> asyncCallbackFechaProceso >>> status 400 >>> '+JSON.stringify(results));//results.opstatus+' '+results.errmsg);
			if (results.opstatus == '0') {
				var resultResponse = results.mtdObtenerFechaProcesoResponse;
				var code = resultResponse[0].intCodError;
				var message = resultResponse[0].strMensaje;
				kony.print('MVP.ModelUtils >>> asyncCallbackFechaProceso >>> code error >>> '+code+' '+message);
				switch (Number(code)) {
					//sesion expirada
					case -1: {
						_info.code = -1;
						results.opstatus = -1;
						_info.message = getErrorConexion(results);;
						kony.print('MVP.ModelUtils >>> asyncCallbackFechaProceso >>> error -1 >>> '+code+' '+message);
					}
					break;
					//exito al obtener fecha de proceso
					case 0: {
						kony.print('MVP.ModelPunto >>> asyncCallbackFechaProceso >>> error 0.1 >>> resultResponse.length >>> '+resultResponse.length);

						_info.code = 0;
						var date = message.stringToDate('dd/mm/yyyy','/')
						_info.message.mensaje = message;
						_info.fechaProceso = date;
					}
					break;
					//error desconocido
					default: {
						_info.code = code;
						_info.message.titulo = 'Fecha de Proceso';
						_info.message.mensaje = message+'.';
						kony.print('MVP.ModelUtils >>> asyncCallbackFechaProceso >>> error default >>> '+code+' '+message);
					}
					break;
				}
				this.emit('changedWsGetFechaProceso', _info);
			} else {
				_info.code = results.opstatus;
				_info.message = getErrorConexion(results);
				kony.print('MVP.ModelUtils >>> asyncCallbackFechaProceso >>> opstatus <> 0 >>> '+_info.code+' '+_info.message);
				this.emit('changedWsGetFechaProceso', _info);
			}
		} else {
			if (status==300) {
				_info.code = results.opstatus;
				_info.message = getErrorConexion(results);
				kony.print('MVP.ModelUtils >>> asyncCallbackFechaProceso >>> status 300 >>> '+_info.code+' '+JSON.stringify(_info.message));
				//this.emit('changedWsGetFechaProceso', _info);
			}
		}
	}

	var ModelUtils = kony.Stapes.subclass({

		'constructor': function() {
			this.init();
		},

		'init': function () {
			_info = {};
			this.handleWSGetFechaProceso = undefined;
		},

	  /**
	   * Get the Previous form
	   */
	  'backApplication': function(frm) {
			switch (frm) {
				case kFrmMobile.LOGIN: {
					initFrmLogin();
					frmLogin.show();
				}
				break;
				case kFrmMobile.MAPA: {
					frmMapa.show();
				}
				break;
				default:{
					var previousForm = kony.application.getPreviousForm();
					previousForm.show();
				}
				break;
			}

			this.emit('changedBackAplication');
		},

	  /**
		 * Cierra la aplicacion y retorna a la pantalla de Login
	   */
	  'exitApplication': function() {
			initFrmLogin();
			frmLogin.show();
			/*
			try{
				kony.application.exit();
			} catch(Error) {
	  		showPopMensaje("Error de Aplicacion","Excepcion mientras se intenta salir de la aplicacion : "+Error);
	  	}
	  	*/
	  	this.emit('changedExitAplication');
		},

		'isNetworkAvailable': function() {
			return kony.net.isNetworkAvailable(constants.NETWORK_TYPE_ANY);
		},

		'wsGetFechaProceso': function() {
			var params = {
				serviceID: 'mtdObtenerFechaProceso',
				id: '0'
			};
			_info.message = {};
			this.handleWSGetFechaProceso = appmiddlewaresecureinvokerasync(
				params,
				asyncCallbackFechaProceso.bind(this)
			);
		},

		'cancelService': function(connHandle) {
			try {
				if (connHandle !== undefined)
		    	kony.net.cancel(connHandle);
			} catch (e) {
		    // todo: handle exception
		    alert(e);
			}
		},

		'isLocationEnabled': function() {
			//#ifdef android
				return UtilsFFI.isLocationServiceEnabled();
			//#endif

			//#ifdef iphone
				return UtilsFFI.isLocationManagerStatus();
			//#endif
		},

		'loadLocationSettings': function() {
			//#ifdef android
				UtilsFFI.loadSettingsLocationService();
			//#endif

			//#ifdef iphone
				UtilsFFI.loadSettingsLocationManager();
			//#endif
		}

	});

	return ModelUtils;

})(kony);
