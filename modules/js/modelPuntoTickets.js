kony.print('MVP.ModelPuntoTickets >>> Loading JS file modelPuntoTickets.js');

var ModelPuntoTickets = (function(kony, undefined) {
  var _puntosTickets;
  var _info;

  function loadListPuntosTickets(list) {
    var puntoTickets = list[0];
    var idPunto = puntoTickets.strIdPunto;
    var pPuntoTickets = {};

    pPuntoTickets = {
      'idPunto': puntoTickets.strIdPunto,
      'horaInicio': puntoTickets.strHoraInicio,
      'horaFin': puntoTickets.strHoraFin,
      'isVisible': puntoTickets.strEstado === '1'? true : false,
      'tickets': new Hashtable()
    };
    kony.print('MVP.ModelPuntoTickets >>> loadListPuntosTickets >>> pPuntoTickets >>> '+idPunto);
    var data = [
        {
          'cantidadDesc': 'Clientes en Fila',
          'tiempoDesc': 'Espera Aprox.'
        },
        [{
          'cantidad': 'Caja: '+puntoTickets.strEsperaCajas,
          'tiempo': Number(puntoTickets.strPromedioCajas)<1? '1 min.' : Number(puntoTickets.strPromedioCajas).formatToString(0, ',', '.') +' min.'
        },
        {
          'cantidad': 'Plataforma Oper.: '+puntoTickets.strEsperaPlataforma,
          'tiempo': Number(puntoTickets.strPromedioPlataforma)<1? '1 min.' : Number(puntoTickets.strPromedioPlataforma).formatToString(0, ',', '.') +' min.'
        },
        {
          'cantidad': 'Ejecutivo de Cuenta: '+puntoTickets.strEsperaEjecutivo,
          'tiempo': Number(puntoTickets.strPromedioEjecutivo)<1? '1 min.' : Number(puntoTickets.strPromedioEjecutivo).formatToString(0, ',', '.') +' min.'
        }]
    ];

    pPuntoTickets.tickets.put(0, data);
    kony.print('MVP.ModelPuntoTickets >>> loadListPuntosTickets >>> pPuntoTickets.tickets.put(0, data) >>> ');
    _puntosTickets.put(idPunto, pPuntoTickets);
    kony.print('MVP.ModelPuntoTickets >>> loadListPuntosTickets >>> _puntosTickets.put(idPunto, pPuntoTickets) >>> ');
  }

  /**
   * async callback ws get puntos tickets
   */
	function asyncCallbackPuntosTickets(status, results) {
		if (status==400) {
			kony.print('MVP.ModelPuntoTickets >>> asyncCallbackPuntosTickets >>> status 400 >>> '+results.opstatus+' '+results.errmsg);
			if (results.opstatus == '0') {
				var resultResponse = results.mtdTicketsEnEsperaResponse;
				var code = resultResponse[0].intCodError;
				var message = resultResponse[0].strMensaje;

				switch (Number(code)) {
					//sesion expirada
					case -1: {
						_info.code = -1;
            results.opstatus = -1;
						_info.message = getErrorConexion(results);;
						kony.print('MVP.ModelPuntoTickets >>> asyncCallbackPuntosTickets >>> error -1 >>> '+code+' '+message);
					}
					break;
					//exito al obtener los puntos
					case 0: {
						kony.print('MVP.ModelPuntoTickets >>> asyncCallbackPuntosTickets >>> error 0.1 >>> resultResponse.length >>> '+resultResponse.length);
						loadListPuntosTickets(resultResponse);
						kony.print('MVP.ModelPuntoTickets >>> asyncCallbackPuntosTickets >>> error 0.2 >>> loadListPuntos.size >>> '+_puntosTickets.size());

						_info.code = 0;
						_info.message = message;
						_info.puntoTickets = _puntosTickets.values();
					}
					break;
					//error desconocido
					default: {
						_info.code = code;
            _info.message.titulo = 'Tickets en Espera';
            _info.message.mensaje = message+'.';
						kony.print('MVP.ModelPuntoTickets >>> asyncCallbackPuntosTickets >>> error default >>> '+code+' '+message);
					}
					break;
				}
				this.emit('changedWsGetPuntosTickets', _info);
			} else {
        _info.code = results.opstatus;
				_info.message = getErrorConexion(results);
        kony.print('MVP.ModelPuntoTickets >>> asyncCallbackPuntosTickets >>> opstatus <> 0 >>> '+_info.code+' '+_info.message);
				this.emit('changedWsGetPuntosTickets', _info);
  		}
		} else {
	  		if (status==300) {
					_info.code = results.opstatus;
					_info.message = getErrorConexion(results);
          kony.print('MVP.ModelPuntoTickets >>> asyncCallbackPuntosTickets >>> status 300 >>> '+_info.code+' '+_info.message);
					//this.emit('changedWsGetPuntosTickets', _info);
	  		}
		}
	}

  var ModelPuntoTickets = kony.Stapes.subclass({

    'constructor': function() {
      this.handleWSGetPuntoTickets = undefined;

      this.init();
    },

    'init': function() {
      _puntosTickets = new Hashtable();
      _info = {};
    },

    'wsGetPuntosTickets': function(params) {
      kony.print("MVP.ModelPuntoTickets >>> wsGetPuntosTickets >>> params >>> "+JSON.stringify(params));
      this.handleWSGetPuntoTickets = appmiddlewaresecureinvokerasync(
  			params,
  			asyncCallbackPuntosTickets.bind(this)
			);
    }

  });

  return ModelPuntoTickets;

})(kony);
