kony.print('MVP.ViewMapa >>> Loading JS file viewMapa.js');

var ViewMapa = (function(kony, undefined) {
  var frmMapa;

  function frmMapaOnPreShow() {
    kony.print('MVP.ViewMapa >>> frmMapaOnPreShow >>> '+this.iPreShow);
    if (this.iPreShow === 0) {
      kony.print('MVP.ViewMapa >>> frmMapaOnPreShow >>> isPreShow >>> geoDefaultPosition');
      //#ifdef iphone
      	this.emit('geoPosition');
      //#else
      	this.emit('geoDefaultPosition');
      //#endif        	  
    }
    this.iPreShow++;
  }

  function hbxImgBackOnClick() {
	  this.emit('backAplicaction');
  }

  function btnExitOnClick() {
		this.emit('exitAplication');
  }

  function mapMapaOnPinClick(mapWgt, location) {
  	kony.print('MVP.mapaMapView >>> mapMapaOnPinClick >>> location >>> '+JSON.stringify(location));

    if (location.desc !== '0') {
      var locationAux, info = {};
			var locationData = frmMapa.mapMapa.locationData;
      for (var i=0, len=locationData.length; i<len; i++) {
        locationAux = locationData[i];
        if (locationAux.name === location.name)
          info.index = i;
      }

      info.location = location;

      this.emit('pinClickMapa', info);
    }
    //frmMapa.mapMapa.locationData[Number(locationData.name)].image = 'ic_pin_atm_a.png';
  }

  function vbxMenuPuntosAtencionOnClick() {
    this.emit('viewMenuOpciones', kOpcionMenuMapa.PUNTOS_ATENCION);
  }

  function vbxMenuVistaMapaOnClick() {
    this.emit('viewMenuOpciones', kOpcionMenuMapa.VISTA_MAPA);
  }

  function vbxMenuPosicionActualOnClick() {
  	this.emit('geoPosition');
  }

  function vbxMenuFiltroPuntosAtencionOnClick() {
    this.emit('viewMenuOpciones', kOpcionMenuMapa.FILTRO_BUSQUEDA);
  }

  /**
   * Class Mapa View
   */
  var ViewMapa = kony.Stapes.subclass({

	  'constructor': function(pfrmMapa) {
      frmMapa = pfrmMapa;

	    this.init();
      this.bindUIEventHandlers();
	  },

	  'init': function () {
      this.iPreShow = 0;

      frmMapa.mapMapa.mode = constants.MAP_VIEW_MODE_NORMAL;
			//#ifdef android
				frmMapa.mapMapa.zoomLevel = 15;
			//#endif

			//#ifdef iphone
				frmMapa.mapMapa.zoomLevel = 14;
			//#endif

			//header
			frmMapa.lblLocalizacion.text = 'Puntos de Atención';
			frmMapa.lblLocalizacionDetalle.text = "";

	    //menu puntos de atencion
			frmMapa.lblPuntosAtencion.text = 'Puntos de Atención';
      this.setEnabledMenuPuntosAtencion(false);
	    //menu vista de mapa
			frmMapa.lblVistaMapa.text = 'Vista de Mapa';
      this.setEnabledMenuVistaMapa(false);
	    //menu mi posicion
			frmMapa.lblPosicionActual.text = 'Mi Posición';
      this.setEnabledMenuPosicionActual(false);
	    //menu filtros de busqueda
			frmMapa.lblFiltroPuntosAtencion.text = 'Filtro de Búsqueda';
			this.setEnabledMenuFiltrosPuntos(false);
	    //kony.application.setApplicationBehaviors({retainSpaceOnHide : true});
	  },

	  /**
	  * Event Handlers for binding this view
	  */
	  'bindUIEventHandlers': function () {
    	frmMapa.preShow = frmMapaOnPreShow.bind(this);

    	frmMapa.hbxImgBack.onClick = hbxImgBackOnClick.bind(this);
			//#ifdef android
				frmMapa.onDeviceBack = hbxImgBackOnClick.bind(this);
			//#endif
			frmMapa.btnExit.onClick = btnExitOnClick.bind(this);

      frmMapa.mapMapa.onPinClick = mapMapaOnPinClick.bind(this);

      frmMapa.vbxMenuPuntosAtencion.onClick = vbxMenuPuntosAtencionOnClick.bind(this);
      frmMapa.vbxMenuVistaMapa.onClick = vbxMenuVistaMapaOnClick.bind(this);
			frmMapa.vbxMenuPosicionActual.onClick = vbxMenuPosicionActualOnClick.bind(this);
      frmMapa.vbxMenuFiltroPuntosAtencion.onClick = vbxMenuFiltroPuntosAtencionOnClick.bind(this);
	  },

	  'show': function() {
	  	frmMapa.show();

  		//#ifdef iphone
	    if (this.iPreShow === 0) {
	      kony.print('MVP.ViewMapa >>> show iPhone >>> isShow >>> '+this.iPreShow);
	  	  this.emit('geoDefaultPosition');
	    }
			//#else
	      if (this.iPreShow === 1) {
	      	kony.print('MVP.ViewMapa >>> show Android >>> isShow >>> '+this.iPreShow);
	        this.emit('geoPosition');
	        //this.iPreShow++;
	        //this.emit('wsGetPuntos');
	      }
			//#endif
	  },

	  'destroy': function() {
	  	frmMapa.destroy();
	  },

	  'refreshLocationData': function(info) {
      kony.print('MVP.mapaMapView >>> refreshLocationData Screen Map >>> info.code >>> '+info.code);
	    kony.print('MVP.mapaMapView >>> refreshLocationData Screen Map >>> info.isZoom >>> '+info.isZoom);
      kony.print('MVP.mapaMapView >>> refreshLocationData Screen Map >>> info.isChangedPosition >>> '+info.isChangedPosition);
      kony.print('MVP.mapaMapView >>> refreshLocationData Screen Map >>> info.isChangedWsGetPuntos >>> '+info.isChangedWsGetPuntos);
    	if (info.code === 0) {
        kony.print('MVP.mapaMapView >>> refreshLocationData Screen Map >>> info.locationData >>> '+JSON.stringify(info.locationData[info.locationData.length-1]));
      	var locationData = info.locationData;

        if (info.isZoom) {
          //#ifdef android
            frmMapa.mapMapa.locationData = locationData;
            frmMapa.mapMapa.zoomLevel = 15; // <+ zoom >-
            frmMapa.mapMapa.navigateTo(frmMapa.mapMapa.locationData.length-1, false);
            kony.print('MVP.mapaMapView >>> refreshLocationData Screen Map >>> ANDROID');
          //#endif
          //#ifdef iphone
            frmMapa.mapMapa.locationData = locationData;
            frmMapa.mapMapa.navigateTo(frmMapa.mapMapa.locationData.length-1, false);
            frmMapa.mapMapa.zoomLevel = 14;
            kony.print('MVP.mapaMapView >>> refreshLocationData Screen Map >>> IOS');
          //#endif
        } else {
          frmMapa.mapMapa.locationData = locationData;
        }

        if (info.isChangedPosition) {
          this.setEnabledMenuVistaMapa(true);
          this.setEnabledMenuPosicionActual(true);
        }

        if (info.isChangedWsGetPuntos) {
          this.setEnabledMenuPuntosAtencion(true);
          this.setEnabledMenuFiltrosPuntos(true);
          frmMapa.mapMapa.navigateTo(frmMapa.mapMapa.locationData.length-1, false);
        }

        this.setIsLoadingMapa(info.isLoadingMapa, info.loadingMessage);
			} else {
			  var message = info.message || null;
			  if (null !== message) {
			    alert(message);
	  		}
			}
	  },

    'getLocationData': function() {
      return frmMapa.mapMapa.locationData;
    },

		'setZoomLevel': function(zoomLevel) {
			frmMapa.mapMapa.zoomLevel = Number(zoomLevel);
		},

		'setNavigateTo': function(index, showcallout) {
			frmMapa.mapMapa.navigateTo(Number(index), showcallout);
		},

    'dismissCallout': function() {
      frmMapa.mapMapa.dismissCallout();
    },

    'setSubtituloMapa': function(isVisible, text) {
      frmMapa.lblLocalizacionDetalle.text = text;
      frmMapa.lblLocalizacionDetalle.isVisible = isVisible;
    },

    'setIsLoadingMapa': function(isLoaging, message) {
      frmMapa.lblMensajeMapa.text = message;
      frmMapa.hbxHeaderMapa.isVisible = isLoaging;
			//frmMapa.hbxMapsMapa.isVisible = !isLoaging;
    },

    'setModeMap': function(modeMap) {
      switch (modeMap) {
        case kModeMap.MAP_VIEW_MODE_HYBRID:
          frmMapa.mapMapa.mode = constants.MAP_VIEW_MODE_HYBRID;
        break;
        case kModeMap.MAP_VIEW_MODE_SATELLITE:
          frmMapa.mapMapa.mode = constants.MAP_VIEW_MODE_SATELLITE;
        break;
        default:
          frmMapa.mapMapa.mode = constants.MAP_VIEW_MODE_NORMAL;
        break;
      }
    },

    'setEnabledMenuPuntosAtencion': function(isEnabled) {
      if (isEnabled) {
      	frmMapa.imgLeftPuntosAtencion.src = "ic_place.png";
        frmMapa.vbxMenuPuntosAtencion.setEnabled(true);
      } else {
      	frmMapa.imgLeftPuntosAtencion.src = "ic_un_place.png";
        frmMapa.vbxMenuPuntosAtencion.setEnabled(false);
      }
    },

    'setEnabledMenuVistaMapa': function(isEnabled) {
      if (isEnabled) {
        frmMapa.imgLeftVistaMapa.src = "ic_map.png";
        frmMapa.vbxMenuVistaMapa.setEnabled(true);
      } else {
      	frmMapa.imgLeftVistaMapa.src = "ic_un_map.png";
        frmMapa.vbxMenuVistaMapa.setEnabled(false);
      }
    },

    'setEnabledMenuPosicionActual': function(isEnabled) {
      if (isEnabled) {
        frmMapa.imgLeftPosicionActual.src = "ic_my_location.png";
        frmMapa.vbxMenuPosicionActual.setEnabled(true);
      } else {
        if (!kony.os.hasGPSSupport())
    			frmMapa.imgLeftPosicionActual.src = "ic_gps_off.png";
    		else
    			frmMapa.imgLeftPosicionActual.src = "ic_gps_search.png";
        frmMapa.vbxMenuPosicionActual.setEnabled(false);
      }
    },

    'setEnabledMenuFiltrosPuntos': function(isEnabled) {
      if (isEnabled) {
      	frmMapa.imgLeftFiltroPuntosAtencion.src = "ic_search.png";
        frmMapa.vbxMenuFiltroPuntosAtencion.setEnabled(true);
      } else {
      	frmMapa.imgLeftFiltroPuntosAtencion.src = "ic_un_search.png";
        frmMapa.vbxMenuFiltroPuntosAtencion.setEnabled(false);
      }
    }

	});

	return ViewMapa;
})(kony);
