function isLocationEnabled() {
	//#ifdef android
		return UtilsFFI.isLocationServiceEnabled();
	//#endif
	
	//#ifdef iphone		
		return UtilsFFI.isLocationManagerStatus();		
	//#endif
}

function loadLocationSettings() {
	//#ifdef android
		UtilsFFI.loadSettingsLocationService();
	//#endif
	
	//#ifdef iphone		
		UtilsFFI.loadSettingsLocationManager();		
	//#endif	
}