kony.print('MVP.PresenterMapa >>> Loading JS file mapaPresenter.js');

var PresenterMapa = (function(kony, undefined) {
  var _selectKeyTipoPunto;
  var _selectKeyDepartamento;
  var _selectIndexLocation;
  var _fechaProceso;

  var isChangedPosition;
  var isChangedPoints;

  var PresenterMapa = kony.Stapes.subclass({

    'constructor': function(pfrmMapa, pPopToastDataLocation, pPopToastOpcionesMapa, pPopPregunta, pPopMensaje) {
      this.viewMapa = new ViewMapa(pfrmMapa);

	    this.modelPunto = new ModelPunto();
      this.modelPuntoTickets = new ModelPuntoTickets();
      this.modelOpcionesMapa = new ModelOpcionesMapa();
      this.modelUtils = new ModelUtils();

      this.popViewOpcionesMapa = new PopViewOpcionesMapa(pPopToastOpcionesMapa, pfrmMapa);
      this.popViewLocation = new PopViewLocation(pPopToastDataLocation, pfrmMapa.hbxHeader);
      this.popViewPregunta = new PopViewPregunta(pPopPregunta);
      this.popViewMensaje = new PopViewMensaje(pPopMensaje);

      this.presenterMapaResumen = new PresenterMapaResumen(new ViewMapaResumen(), this.modelPunto, pPopPregunta, pPopMensaje);

      this.init();
	  },

    'init': function() {
      this.watchViewMapa();
      this.watchPopViewLocation();
      this.watchPopViewOpcionesMapa();
      this.watchPopViewPregunta();
      this.watchPopViewMensaje();

      this.watchModelPunto();
      this.watchModelPuntoTickets();
      this.watchModelOpcionesMapa();
      this.watchModelUtils();

      this.watchPresenterMapaResumen();

			this.viewMapa.init();

      _selectKeyTipoPunto = 'T';
      _selectKeyDepartamento = '7'
      _selectIndexLocation = 0;

      isChangedPosition = false;
      isChangedPoints = false;

      this.modelOpcionesMapa.getMenuPuntoAtencion();
      this.modelOpcionesMapa.getMenuFiltroBusqueda();
    },

	  'setViewMapa': function(pViewMapa) {
			this.viewMapa = pViewMapa;
			this.watchViewMapa();
	  },

	  'setModelPunto': function(pModelPunto) {
			this.modelPunto = pModelPunto;
			this.watchModelPunto();
	  },

	  /**
	   * Respond to events emitted by the view mapa
	   */
	  'watchViewMapa': function() {
      /**
       * event's handler's
       */
      this.viewMapa.on({

	      'exitAplication': function() {
	      	kony.print('MVP.PresenterMapa >>> Ask PreguntaView to show');
          this.popViewPregunta.setTituloAndMensaje(
              AppConfig.APP_NAME,
              MensajePop.CERRAR_SESION,
              kPregunta.EXIT_APP
          );
	      	this.popViewPregunta.show();
	      },

	      'backAplicaction': function() {
	      	kony.print('MVP.PresenterMapa >>> Ask UtilsModel to backApplication');
	      	this.modelUtils.backApplication(kFrmMobile.LOGIN);
	      },

	      'geoDefaultPosition': function() {
	      	kony.print('MVP.PresenterMapa >>> Ask PuntosModel to geoDefaultPosition');
	        this.modelPunto.geoDefaultPosition();
	      },

	      'geoPosition': function() {
          kony.print('MVP.PresenterMapa >>> Ask UtilsModel to isLocationEnabled >>> '+this.modelUtils.isLocationEnabled());
          if (this.modelUtils.isLocationEnabled()) {
            kony.print('MVP.PresenterMapa >>> Ask PuntosModel to geoPosition');
            isChangedPosition = false;

            this.viewMapa.setIsLoadingMapa(true, 'obteniendo posicion...');
            this.viewMapa.setEnabledMenuPosicionActual(false);
  	        this.modelPunto.geoPosition(60000, true);
          } else {
            this.popViewPregunta.setTituloAndMensaje(
              'Puntos de Atención',
  					  'Parece que el servicio de ubicación está apagado, activalo para mostrar los puntos de atención.',
              kPregunta.LOCATION_SERVICE
            );
            this.popViewPregunta.show();
          }
	      },

        'wsGetFechaProceso': function() {
          this.modelUtils.wsGetFechaProceso();
        },

	      'wsGetPuntos': function() {
          kony.print('MVP.PresenterMapa >>> Ask PuntosModel to wsGetPuntos');
          isChangedPoints = false;
	        this.modelPunto.wsGetPuntos(gPersona.intNroPersona, _selectKeyTipoPunto);
	      },

        'viewMenuOpciones': function(opcion) {
          kony.print('MVP.PresenterMapa >>> Ask PopViewOpcionesMapa to show');
          this.popViewOpcionesMapa.refreshMenuOpciones(opcion);
          this.popViewOpcionesMapa.show();
        },

        'pinClickMapa': function(info) {
          kony.print('MVP.PresenterMapa >>> Ask PuntosModel to setSelectPunto >>> '+JSON.stringify(info.location));
          //this.modelPunto.setSelectLocation(locationData);
          _selectIndexLocation = info.index;

          this.modelPunto.setSelectPunto(info.location);
          var selectLocation = this.modelPunto.getSelectPunto();
          kony.print('MVP.PresenterMapa >>> selectLocation >>> '+JSON.stringify(selectLocation));
          this.popViewLocation.loadLocation(selectLocation);
          this.popViewLocation.show();
          kony.print('MVP.PresenterMapa >>> Ask PopViewLocation to show >>> ');

        	this.viewMapa.setEnabledMenuPuntosAtencion(false);
          this.viewMapa.setEnabledMenuVistaMapa(false);
          this.viewMapa.setEnabledMenuPosicionActual(false);
          this.viewMapa.setEnabledMenuFiltrosPuntos(false);
        }

	    }, this);

	  },

    'watchPopViewLocation': function() {

      this.popViewLocation.on({

        'closePopViewLocation': function() {
          kony.print('MVP.PresenterMapa >>> Ask popViewLocation to dismiss >>> ');
          this.popViewLocation.dismiss();
        },

        'wsGetPuntosTickets': function(pLocation) {
          if (this.modelUtils.isNetworkAvailable()) {
            var params = {
              serviceID: 'mtdTicketsEnEspera',
              codusuario: encrypt(gPersona.intNroPersona)+'',
              pIntPuntos: pLocation.idPunto
            };
            this.modelPuntoTickets.wsGetPuntosTickets(params);
          } else {
            var results = {intCodError:'1011'};
            var message = getErrorConexion(results);
            this.popViewLocation.setTextLblClientesEspera(message.mensaje);
            /*
            this.popViewMensaje.setTituloAndMensaje(
              message.titulo,
              message.mensaje
            );
            this.popViewMensaje.show();
            */
          }
        },

        'viewMapaResumen': function() {
          //this.modelUtils.cancelService(this.modelPuntoTickets.handleWSGetPuntoTickets);
          kony.print('MVP.PresenterMapa >>> Ask popViewLocation to dismiss >>> ');
          this.popViewLocation.dismiss();
          //this.presenterMapaResumen.viewMapaResumen.loadLocationResumen(this.modelPunto.getSelectPunto());

          this.presenterMapaResumen.setModelPunto(this.modelPunto);
          this.presenterMapaResumen.viewMapaResumen.init();
          this.presenterMapaResumen.viewMapaResumen.loadHeaderPuntoResumen(this.modelPunto.getSelectPunto());
          this.presenterMapaResumen.viewMapaResumen.loadPuntoPropiedades('cargando...', true);

          this.popViewPregunta.dismiss();
          this.popViewMensaje.dismiss();
          this.popViewOpcionesMapa.dismiss();

          this.presenterMapaResumen.viewMapaResumen.show();
        },

        'hidePopViewLocation':function() {
          kony.print('MVP.PresenterMapa >>> Ask popViewLocation to hidePopViewLocation >>> ');
          this.modelUtils.cancelService(this.modelPuntoTickets.handleWSGetPuntoTickets);

          kony.print('MVP.PresenterMapa >>> Ask viewMapa to dismissCallout >>> ');
          this.viewMapa.dismissCallout();

          this.viewMapa.setEnabledMenuPuntosAtencion(true);
          this.viewMapa.setEnabledMenuVistaMapa(true);
          this.viewMapa.setEnabledMenuPosicionActual(true);
          this.viewMapa.setEnabledMenuFiltrosPuntos(true);
        }

      }, this);

    },

    'watchPopViewOpcionesMapa': function() {

      this.popViewOpcionesMapa.on({

        'selectedPuntosAtencion': function(info) {
          _selectKeyTipoPunto = info.key;
          kony.print('MVP.PresenterMapa >>> Ask ModelPunto to getLocationDataByTipoPunto');
          this.modelPunto.getLocationDataByTipoPunto(_selectKeyTipoPunto);
        },

        'changedModeMap': function(modeMap) {
          this.viewMapa.setModeMap(modeMap);
          this.popViewOpcionesMapa.dismiss();
        },

        'selectedFiltroBusqueda': function(info) {
          _selectKeyDepartamento = info.key;
          kony.print('MVP.PresenterMapa >>> Ask ModelPunto to selectedFiltroBusqueda');
          this.modelPunto.getIdLocationByDpto(_selectKeyDepartamento, this.viewMapa.getLocationData());
        }

      }, this);

    },

		/**
	   * Respond to events emitted by the popView Pregunta
	   */
	  'watchPopViewPregunta': function(){
      /**
       * event's handler's
       */
      this.popViewPregunta.on({

	      'yesExitApp': function() {
	      	kony.print('MVP.PresenterMapa >>> Ask UtilModel to exitApplication');
	      	this.modelUtils.exitApplication();
	      },

        'yesLocationService': function() {
          kony.print('MVP.PresenterMapa >>> Ask UtilModel to loadLocationSettings');
          this.modelUtils.loadLocationSettings();
          this.popViewPregunta.dismiss();
        },

	      'no': function() {
	      	kony.print('MVP.PresenterMapa >>> Ask PreguntaView to dismiss');
	      	this.popViewPregunta.dismiss();
	      }

	    }, this);

	  },

    'watchPopViewMensaje': function() {

      this.popViewMensaje.on({
        'ok': function() {
          kony.print('MVP.PresenterMapa >>> Ask MensajeView to dismiss');
          this.popViewMensaje.dismiss();
        }
      }, this);

    },

	  /**
	   * Respond to events emitted by the model punto
	   */
	  'watchModelPunto': function() {
      /**
       * event's handler's
       */
      this.modelPunto.on({
	      'changedDefaultPosition': function(info) {
	      	kony.print('MVP.PresenterMapa >>> Ask ViewMapa to refreshLocationData DEFAULT');
          info.isZoom = true;
          info.isChangedPosition = false;
          info.isChangedWsGetPuntos = false;
          info.isLoadingMapa = true;
          info.loadingMessage = 'obteniendo posición...';

          this.viewMapa.refreshLocationData(info);
	      },

	      'changedPosition': function(info) {
	        kony.print('MVP.PresenterMapa >>> Ask ViewMapa to refreshLocationData GEO >>> iPreShow >>>'+this.viewMapa.iPreShow);
          if (info.code === 0) {
            isChangedPosition=true;

            if (isChangedPoints) {
              this.modelPunto.calculateDistance();
              _selectKeyDepartamento = this.modelPunto.getIdDpto();
              this.popViewOpcionesMapa.setIndexSelectedFiltroBusqueda(_selectKeyDepartamento);
            }

            info.isZoom = true;
            info.isChangedPosition = true;
            info.isChangedWsGetPuntos = false;
            info.isLoadingMapa = !isChangedPoints || !isChangedPosition;
            info.loadingMessage = !isChangedPoints? 'cargando puntos...' : 'obteniendo posición...';

            this.viewMapa.refreshLocationData(info);

            if (this.viewMapa.iPreShow === 1) {
              if (this.modelUtils.isNetworkAvailable()) {
                this.viewMapa.iPreShow++;
                kony.print('MVP.PresenterMapa >>> Ask modelUtils to wsGetFechaProceso >>>');
                this.viewMapa.emit('wsGetFechaProceso');
              } else {
                var results = {intCodError:"1011"};
                var message = getErrorConexion(results);
                this.viewMapa.setIsLoadingMapa(true, 'Error al actualizar los puntos...');
                this.popViewMensaje.setTituloAndMensaje(
                  message.titulo,
                  message.mensaje
                );
                this.popViewMensaje.show();
              }
            }
          } else {
            this.viewMapa.setIsLoadingMapa(true, 'Error al obtener posición');
            this.popViewMensaje.setTituloAndMensaje(
              'Puntos de Atención',
              info.message
            );
            this.popViewMensaje.show();
          }
	      },

	      'changedWsGetPuntos': function(info) {
	      	kony.print('MVP.PresenterMapa >>> Ask ViewMapa to refreshLocationData WS >>> info.code >>> '+info.code);
          if (info.code === 0) {
            isChangedPoints = true;

            if (isChangedPosition) {
              this.modelPunto.calculateDistance();
              _selectKeyDepartamento = this.modelPunto.getIdDpto();
              this.popViewOpcionesMapa.setIndexSelectedFiltroBusqueda(_selectKeyDepartamento);
            }

            info.isZoom = false;
            info.isChangedPosition = false;
            info.isChangedWsGetPuntos = true;
            info.isLoadingMapa = !isChangedPosition;
            info.loadingMessage = !isChangedPosition? 'obteniendo posición...': '';

            var oPuntoAtencion = this.modelOpcionesMapa.findTipoPuntoAtencion(info.tipoPunto);
            this.viewMapa.refreshLocationData(info);
            this.viewMapa.setSubtituloMapa(true, oPuntoAtencion.descripcion);
            //mostrar menu puntos atencion
            this.popViewOpcionesMapa.setSelectedPuntosAtencion(oPuntoAtencion.key);
            this.popViewOpcionesMapa.refreshMenuOpciones(kOpcionMenuMapa.PUNTOS_ATENCION);
            this.popViewOpcionesMapa.show();
          } else {
            this.viewMapa.setIsLoadingMapa(true, 'Error al actualizar los puntos...');
            this.popViewMensaje.setTituloAndMensaje(
              info.message.titulo,
              info.message.mensaje
            );
            this.popViewMensaje.show();
          }

	      },

        'changedGetLocationData': function(info) {
          kony.print('MVP.PresenterMapa >>> Ask ViewMapa to refreshLocationData TIPO_PUNTO >>> info.tipoPunto >>> '+info.tipoPunto);
          info.isZoom = false;
          info.isChangedPosition = false;
          info.isChangedWsGetPuntos = false;
          info.isLoadingMapa = !isChangedPosition;
          info.loadingMessage = !isChangedPosition? 'obteniendo posición...': '';

          var oPuntoAtencion = this.modelOpcionesMapa.findTipoPuntoAtencion(info.tipoPunto);
          this.viewMapa.refreshLocationData(info);
          this.viewMapa.setSubtituloMapa(true, oPuntoAtencion.descripcion);

          this.modelPunto.getIdLocationByDpto(_selectKeyDepartamento, this.viewMapa.getLocationData());

          this.popViewOpcionesMapa.dismiss();
        },

        'changedIdLocationByIdDpto': function(info) {
          if (info.idLocation != -1) {
            this.viewMapa.setNavigateTo(info.idLocation, false);

            this.popViewOpcionesMapa.dismiss();
          } else {
            this.popViewMensaje.setTituloAndMensaje(
              'Puntos de Atención',
              'En este departamento recomendamos utilizar cajeros automáticos de las redes Plus y Redbank para retiro de fondos.'
            );
            this.popViewMensaje.show();
          }
        }

	    }, this);
	  },

    /**
     * Respond to events emitted by the mmodel puntos tickets
     */
    'watchModelPuntoTickets': function() {
      this.modelPuntoTickets.on({

        'changedWsGetPuntosTickets': function(info) {
          if (info.code === 0) {
            kony.print('MVP.PresenterMapa >>> Ask PopViewLocation to loacLocationTickets >>> '+JSON.stringify(info.puntoTickets));
            kony.print('MVP.PresenterMapa >>> Ask PopViewLocation to loacLocationTickets >>> '+JSON.stringify(this.modelPunto.getSelectPunto()));
            this.popViewLocation.loadLocationTickets(info.puntoTickets, this.modelPunto.getSelectPunto());
          } else {
            this.popViewLocation.setTextLblClientesEspera(info.message.mensaje);
          }
        }

      }, this);
    },

    /**
     * Respond to events emitted by the mmodel opcionesMapa
     */
    'watchModelOpcionesMapa': function() {
      /**
       * event's handler's
       */
      this.modelOpcionesMapa.on({

        'changedMenuPuntosAtencion': function(info) {
          var data = info.opcionesPuntosAtencion;
          kony.print('MVP.PresenterMapa >>> Ask PopViewOpcionesMapa to refreshMenuPuntosAtencion >>> data.length >>>'+data.length);
          this.popViewOpcionesMapa.refreshMenuPuntosAtencion(data);
        },

        'changedMenuFiltroBusqueda': function(info) {
          var data = info.opcionesFiltroBusqueda;
          kony.print('MVP.PresenterMapa >>> Ask PopViewOpcionesMapa to refreshMenuFiltroBusqueda >>> data.length >>>'+data.length);
          this.popViewOpcionesMapa.refreshMenuFiltroBusqueda(data);
        }

      }, this);

    },

		/**
     * Respond to events emitted by the mmodel utils
     */
	  'watchModelUtils': function() {
      /**
       * event's handler's
       */
      this.modelUtils.on({
	      'changedExitAplication': function() {
	        kony.print('MVP.PresenterMapa >>> Ask PregutaView to dismiss >>> Ask MapaView to destroy');
          this.modelUtils.cancelService(this.modelUtils.handleWSGetFechaProceso);
          this.modelUtils.cancelService(this.modelPunto.handleWSGetPuntos);
          this.modelUtils.cancelService(this.modelPunto.handleWSGetPuntoPropiedades);
          this.popViewPregunta.dismiss();
          this.popViewMensaje.dismiss();
          this.popViewOpcionesMapa.dismiss();
	        //this.viewMapa.destroy();
	      },

	      'changedBackAplication': function() {
	      	kony.print('MVP.PresenterMapa >>> Ask MapaView to hide');
	      	this.modelUtils.cancelService(this.modelUtils.handleWSGetFechaProceso);
          this.modelUtils.cancelService(this.modelPunto.handleWSGetPuntos);
          this.modelUtils.cancelService(this.modelPunto.handleWSGetPuntoPropiedades);
          this.popViewMensaje.dismiss();
          this.popViewOpcionesMapa.dismiss();
	      	//this.viewMapa.destroy();
	      },

        'changedWsGetFechaProceso': function(info) {
          if (info.code === 0) {
            kony.print('MVP.PresenterMapa >>> Ask viewMap to wsGetPuntos & Ask modelPunto to setFechaProceso >>> '+JSON.stringify(info.fechaProceso));
            this.modelPunto.setFechaProceso(info.fechaProceso);
            this.viewMapa.emit('wsGetPuntos');
          } else {
            this.viewMapa.setIsLoadingMapa(true, 'Error al obtener los puntos...');
          }
        }

	    }, this);

	  },


    'watchPresenterMapaResumen': function() {

      this.presenterMapaResumen.on({

        'changedBackMapaResumen': function() {
          kony.print('MVP.PresenterMapa >>> Ask MapaView to setNavigateTo');
          this.viewMapa.setNavigateTo(_selectIndexLocation, false);
        }

      }, this);
    }

	});

	return PresenterMapa;
})(kony);
