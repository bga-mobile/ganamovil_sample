kony.print('MVP.PopViewPregunta >>> Loading JS file popViewPregunta.js');

/**
 * Class Pregunta View
 */
var PopViewPregunta = (function(kony, undefined) {

	var popPregunta;
	var tipoPregunta;

	function btnYesOnClick() {
		kony.print('MVP.PopViewPregunta >>> btnYesOnClick >>> '+tipoPregunta);
		switch (Number(tipoPregunta)) {
			case kPregunta.ENROLAMIENTO: {
				kony.print('MVP.PopViewPregunta >>> btnYesOnClick >>> yesLoginEnrolamiento');
				this.emit('yesEnrolamiento');
			}
			break;
			case kPregunta.DEVICE_BACK: {
				kony.print('MVP.PopViewPregunta >>> btnYesOnClick >>> yesDashboardDeviceBack');
				this.emit('yesDeviceBack');
			}
			break;
			case kPregunta.LOCATION_SERVICE: {
				kony.print('MVP.PopViewPregunta >>> btnYesOnClick >>> yesLocationService');
				this.emit('yesLocationService');
			}
			break;
			case kPregunta.EXIT_APP: {
				kony.print('MVP.PopViewPregunta >>> btnYesOnClick >>> yesExitApplicaction');
				this.emit('yesExitApp');
			}
			break;
			default: {
				kony.print('MVP.PopViewPregunta >>> btnYesOnClick >>> default');
				this.emit('no');
			}
			break;
		}
	}

	function btnNoOnClick() {
		this.emit('no');
	}

	var PopViewPregunta = kony.Stapes.subclass({

		'constructor': function (pPopPregunta) {
			popPregunta = pPopPregunta;
			tipoPregunta = kPregunta.NOTHING;

			this.bindUIEventHandlers();
		},

		'init': function () {
			popPregunta.lblTitulo.text = this.titulo;
			popPregunta.lblMensaje.text = this.mensaje;
		},

	  /**
	  * Event Handlers for binding this view
	  */
	  'bindUIEventHandlers' : function () {
	  	popPregunta.btnYes.onClick = btnYesOnClick.bind(this);
	  	popPregunta.btnNo.onClick = btnNoOnClick.bind(this);
	  },

	  'show': function () {
			this.init();
	  	popPregunta.show();
	  },

	  'dismiss': function () {
	  	popPregunta.dismiss();
	  },

		'setTituloAndMensaje': function (pTitulo, pMensaje, pTipoPregunta) {
			tipoPregunta = pTipoPregunta;
			popPregunta.btnYes.onClick = btnYesOnClick.bind(this);
			this.titulo = pTitulo;
			this.mensaje = pMensaje;
		}

	});

	return PopViewPregunta;

})(kony);
