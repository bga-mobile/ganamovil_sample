/**
 * Convierte una cadena en formato titulo
 * Ej: "hola mundo" -> "Hola Mundo"
 * @returns {this}
 */
String.prototype.capitalize = function() {
	return this.replace(/\w\S*/g,
		function(txt){
			return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();
		}
	);
};

/**
 * Quita los todos los espacions > 1
 * Ej: " Hola    Mundo   " -> "Hola Mundo"
 * @returns {this}
 */
String.prototype.trimAll = function() {
	return this.replace(/([\ \t]+(?=[\ \t])|^\s+|\s+$)/g, '');
};

/**
 * Quita todos los caracteres que no sean [0-9], ".", "," o " "
 * Ej: " Hola4   45  Mundo 4,000.00  " -> " 4   45   4,000.00  "
 * @returns {this}
 */
String.prototype.onlyNumber = function() {
	var x = "";

	for (var i = 0; i < this.length; i++) {
		/*
		if (Number(this.charAt(i)>=0) || this.charAt(i)=="." ||
		    this.charAt(i)=="," || this.charAt(i)== " ")
		*/
		if (Number(this.charAt(i)>=0) || this.charAt(i)=="." || this.charAt(i)== " ")
			x = x + this.charAt(i);
	}

	return x;
};

String.prototype.isAlfaNumber = function() {
	var rgx = new RegExp(/^[a-zA-Z0-9\sÑÁÉÍÓÚñáéíóú]*$/);
	return rgx.test(this);
};

String.prototype.stringToDate = function(format, separador) {
	var d = this;
	var from = d.split(separador);
	if ('dd/mm/yyyy' || 'dd-mm-yyyy')
		return new Date(Number(from[2]), Number(from[1]) - 1, Number(from[0]));
	else
		if ('yyyy/mm/dd' || 'yyyy-mm-dd')
			return new Date(Number(from[0]), Number(from[1]) - 1, Number(from[2]));
		else
			return new Date();
}
