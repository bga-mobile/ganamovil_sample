kony.print('MVP.ModelPunto >>> Loading JS file modelPunto.js');

var ModelPunto = (function(kony, undefined) {
  var _locationData;
  var _puntosData;
  var _info;

  var _tipoPunto;
  var _idDpto;
  var _puntoSelected;

  var _fechaProceso;

  /**
   * get imagen de acuerdo al tipo, subtipo y si es central
   * @param {char} tipo
   * @param {char} subtipo
   * @param {boolean} isCentral
   * @return {String} img
   */
  function getImagen(tipo, subTipo, isCentral, isSelected) {
		var img = '';

		if (isCentral && tipo == 'A' && subTipo == 'A')
			subTipo = 'C';

		switch (subTipo) {
			case 'P':
				img = isSelected? 'ic_pin_person2.png' : 'ic_pin_person2.png';
			break;
			//atm retiro
			case 'T':
				img = isSelected? 'ic_pin_atm.png' : 'ic_pin_atm_a.png';
			break;
			//atm retiro/deposito
			case 'E':
				img = isSelected? 'ic_pin_atm_dep.png' : 'ic_pin_atm_dep.png';
			break;
			//atm retiro discapacitados
			case 'I':
				img = isSelected? 'ic_pin_atm_dis.png' : 'ic_pin_atm_dis.png';
			break;
			//atm retiro/deposito discapacitados
			case 'J':
				img = isSelected? 'ic_pin_atm_dis.png' : 'ic_pin_atm_dis.png';
			break;
			//agencia
			case 'A':
				img = isSelected? 'ic_pin_agencia.png' : 'ic_pin_agencia.png';
			break;
			//agencia central
			case 'C':
				img = isSelected? 'ic_pin_sucursal2.png' : 'ic_pin_sucursal2.png';
			break;
			//agencia autobanco
			case 'B':
				img = isSelected? 'ic_pin_auto.png' : 'ic_pin_auto.png';
			break;
			//agencia desplazada
			case 'D':
				img = isSelected? 'ic_pin_desplazada.png' : 'ic_pin_desplazada.png';
			break;
		}

		return img;
	}

  /**
   * This routine calculates the distance between two points (given the
   * latitude/longitude of those points). It is being used to calculate
   * the distance between two locations using GeoDataSource (TM) prodducts
   *
   * Definitions:
   *   South latitudes are negative, east longitudes are positive
   *
   * Passed to function:
   *   lat1, lon1 = Latitude and Longitude of point 1 (in decimal degrees)
   *   lat2, lon2 = Latitude and Longitude of point 2 (in decimal degrees)
   *   unit = the unit you desire for results
   *          where: 'M' is statute miles (default)
   *                 'K' is kilometers
   *                 'N' is nautical miles
   *
   * Worldwide cities and other features databases with latitude longitude
   * are available at http://www.geodatasource.com
   *
   * For enquiries, please contact sales@geodatasource.com
   *
   * Official Web site: http://www.geodatasource.com
   *
   *             GeoDataSource.com (C) All Rights Reserved 2015
   */
  function distance(lat1, lon1, lat2, lon2, unit) {
    var radlat1 = Math.PI * lat1/180;
  	var radlat2 = Math.PI * lat2/180;
  	var theta = lon1-lon2;
  	var radtheta = Math.PI * theta/180;
  	var dist = Math.sin(radlat1) * Math.sin(radlat2) + Math.cos(radlat1) * Math.cos(radlat2) * Math.cos(radtheta);

  	dist = Math.acos(dist);
  	dist = dist * 180/Math.PI;
  	dist = dist * 60 * 1.1515;

  	if (unit=="K") {
  		dist = dist * 1.609344;
  	}
  	if (unit=="N") {
  		dist = dist * 0.8684;
  	}

  	return dist;
  }

  /**
   * put location
   * @param {String} lat
   * @param {String} lon
   * @param {String} name
   * @param {String} desc
   * @param {String} image
   * @param {boolean} isShow
   */
  function putLocation(key, location) {
    try {
			var value = {
				'lat': location.lat+'',
				'lon': location.lon+'',
				'name': location.name,
				'desc': location.desc,
				'image': location.image,
				'showcallout': location.showcallout
	    };
	    _locationData.put(key, value);
		} catch (error) {
			_info.code = 1;
			_info.message = 'Error: al poner mi posición actual falló.';
		} finally {
	    _info.code = 0;
    	_info.message = 'Se actualizo la localizacion';
		}
  }

  function putPunto(key, punto) {
    try {
			var value = {
				'idMapa': punto.idMapa,
				'idPunto': punto.idPunto,
				'idTipo': punto.idTipo,
				'descTipo': punto.descTipo,
				'idSubTipo': punto.idSubTipo,
				'nombre': punto.nombre,
				'direccion': punto.direccion,
				'latitud': punto.latitud,
				'longitud': punto.longitud,
				'idDpto': punto.idDpto,
				'descDpto': punto.descDpto,
				'telefono': punto.telefono,
				'distancia': punto.distancia,
				'propiedades': punto.propiedades,
				'isCentral': punto.isCentral,
				'isVisible': punto.isVisible,
				'isSelected': punto.isSelected
	    };
	    _puntosData.put(key, punto);
		} catch (error) {
			_info.code = 1;
			_info.message = 'Error: al poner punto de atencion falló.';
		} finally {
	    _info.code = 0;
    	_info.message = 'Se actualizo el punto';
		}
  }

  /**
   * load List puntos (puntos)
   * @param {list} list
	 * @param {char} tipoPunto
   */
  function loadListPuntos(list) {
		var punto = {};
		var pPunto = {};
    var propiedades = {};
    var pPropiedades = {};

    var fechaInicioS = '';
    var fechaFinS = '';

    var fechaInicio = new Date();
    var fechaFin = new Date();

    kony.print('MVP.PuntosModel >>> loadListPuntos >>> _fechaProceso >>> '+_fechaProceso);

		for (var i = 0, len = list.length; i < len; i++) {
			punto = list[i];

      fechaInicioS = punto.strFechaVistaI;
      fechaFinS = punto.strFechaVistaF;

      fechaInicio = fechaInicioS.stringToDate('dd/mm/yyyy','/');
      fechaFin = fechaFinS.stringToDate('dd/mm/yyyy','/');

			pPunto = {
				'idMapa': i+1,
				'idPunto': punto.intIdPunto,
				'idTipo': punto.intTipoPunto,
        'descTipo': punto.idTipo == 'A'? 'Sucursal/Agencia': 'Cajero Automático',
				'idSubTipo': punto.strSubtipo_Punto,
				'nombre': punto.strNombre,
				'direccion': punto.strDireccion,
				'latitud': punto.strLatitud,
				'longitud': punto.dblLongitud,
				'idDpto': punto.strPlaza,
				'descDpto': punto.strDescripcion,
				'telefono': punto.strTelefono.length>0? punto.strTelefono : 'Sin Número',
        'distancia': 0,
				'propiedades': new Hashtable(),
        'isCentral': punto.strCentral == 'S'? true : false,
        'isVisible': (punto.strVisible == 'S' &&
                      _fechaProceso >= fechaInicio &&
                      _fechaProceso <= fechaFin
                     ) ? true : false,
				'isSelected': true
			};

			putPunto(i+1, pPunto);
		}
  }

  function loadListPuntosPropiedades(list) {
    var lPropiedades = {};

    for (var k = list.length-1; k>=0; k--) {
      lPropiedades = {
        'key': list[k].intIdPropiedad+'',
        'clave': list[k].intIdClave,
        'valor': list[k].strValor
      };
      _puntoSelected.propiedades.put(list[k].intIdPropiedad+'', lPropiedades);
    }

    lPropiedades = {
      'key': '0',
      'clave': 'Dirección',
      'valor': _puntoSelected.direccion
    };
    _puntoSelected.propiedades.put('0', lPropiedades);

    //kony.print('MVP.PuntosModel >>> loadListPuntosPropiedades >>> '+JSON.stringify(_puntoSelected.propiedades.values()));

    putPunto(_puntoSelected.idMapa, _puntoSelected);
  }

  /**
   * @param {char} idTipo
   */
  function loadPuntosMap(idTipo) {
    _locationData.clear();

    kony.print('MVP.PuntosModel >>> puntosData('+ 0 +') >>> '+JSON.stringify(_puntosData.get(0)));

    var TIPOS_PUNTOS;
    switch (idTipo) {
      case 'A':
        TIPOS_PUNTOS = ['A','P'];
      break;
      case 'T':
        TIPOS_PUNTOS = ['T','P'];
      break;
      default:
        TIPOS_PUNTOS = ['A','T','P'];
      break;
    }

    kony.print('MVP.PuntosModel >>> loadPuntosMap >>> puntosData.size() >>> '+_puntosData.size());
    var punto = {};
    for (var i = 0, len = _puntosData.size(); i<len; i++) {
      punto = _puntosData.get(i);
      //kony.print('MVP.PuntosModel >>> geoDefaultPosition >>> '+ i +' >>> '+JSON.stringify(punto));
      //kony.print('MVP.PuntosModel >>> loadPuntosMap >>> '+ i +' >>> visible>>> '+punto.isVisible+' >>> '+JSON.stringify(punto));
      if (TIPOS_PUNTOS.indexOf(punto.idTipo) >= 0) {
        if (punto.isVisible) {
          var location = {
            'lat': punto.latitud,
            'lon': punto.longitud,
            'name': punto.idPunto+'',
            'desc': punto.idMapa+'',
            'image': getImagen(punto.idTipo, punto.idSubTipo, punto.isCentral, punto.isSelected),
            'showcallout': false
          };
          putLocation(i, location);
        }
      }
    }
  }

  function compareASC(locationA, locationB){
      return parseFloat(locationA.distancia) - parseFloat(locationB.distancia);
  }

  /**
   * callbacks success para geoPosition
   * @param {object} position
   */
  function geoSuccessCallBack(position) {
		var lat = position.coords.latitude;
		var lon = position.coords.longitude;

		if ((lat!==null || !isNaN(lat)) && (lon!==null || !isNaN(lon))) {
			var location = {
				'lat': lat,
				'lon': lon,
				'name': '0',
				'desc': '0',
				'image': getImagen('P','P',true, true),
				'showcallout': false
			};
			putLocation(0, location);

			var pPunto = {
				'idMapa': 0,
				'idPunto': 0,
				'idTipo': 'P',
        'descTipo': 'Mi Posición Actual',
				'idSubTipo': 'P',
				'nombre': '0',
				'direccion': '0',
				'latitud': lat,
				'longitud': lon,
				'idDpto': 0,
				'descDpto': 'Mi Posición Actual',
				'telefono': '0',
        'distancia': 0,
				'propiedades': new Hashtable(),
				'isCentral': false,
				'isVisible': true,
				'isSelected': true
			};
			putPunto(0, pPunto);

	    if (_info.code === 0) {
				_info.locationData = _locationData.values();
        kony.print('MVP.ModelPunto >>> geoSuccessCallBack >>> puntosData(0) >>> '+JSON.stringify(_puntosData.get(0)));
        kony.print('MVP.ModelPunto >>> geoSuccessCallBack >>> locationData(0) >>> '+JSON.stringify(_locationData.get(0)));
      } else {
        _info.code = 1;
    		_info.message = 'Error: al traer la posición actual falló.';
      }
  	} else {
  		_info.code = 1;
  		_info.message = 'Error: al traer la posición actual falló.';
  	}
    kony.print('MVP.ModelPunto >>> geoSuccessCallBack >>> code message >>> '+_info.code+' '+_info.message);
  	this.emit('changedPosition', _info);
	}

	/**
	 * callBack error para geoPosition
	 */
  function geoErrorCallBack(positionError) {
    _info.code = 1;
		_info.message = "Error: "+positionError.code+'\n'+positionError.message;

    this.emit('changedPosition', _info);
  }

  /**
   * async callback ws get puntos
   */
  function wsGetPuntosStatic() {
    var resultResponse = PuntosResult.mtdObtenerPuntosResponse;

    kony.print('MVP.ModelPunto >>> wsGetPuntosStatic >>> resultResponse.length >>> '+resultResponse.length);
    loadListPuntos(resultResponse);
    kony.print('MVP.ModelPunto >>> wsGetPuntosStatic >>> loadListPuntos.size >>> '+_puntosData.size());
    loadPuntosMap(_tipoPunto);
    kony.print('MVP.ModelPunto >>> wsGetPuntosStatic >>> locationData.size >>> '+_locationData.size());

    _info.code = 0;
    _info.message.mensaje = '';
    _info.tipoPunto = _tipoPunto;
    _info.locationData = _locationData.values();

    this.emit('changedWsGetPuntos', _info);
  }

	function asyncCallbackPuntos(status, results) {
		if (status==400) {
			kony.print('MVP.ModelPunto >>> asyncCallbackPuntos >>> status 400 >>> '+JSON.stringify(results));//results.opstatus+' '+results.errmsg);
			if (results.opstatus == '0') {
				var resultResponse = results.mtdObtenerPuntosResponse;
				var code = resultResponse[0].intCodError;
				var message = resultResponse[0].strMensaje;

				switch (Number(code)) {
					//sesion expirada
					case -1: {
						_info.code = -1;
            results.opstatus = -1;
						_info.message = getErrorConexion(results);;
						kony.print('MVP.ModelPunto >>> asyncCallbackPuntos >>> error -1 >>> '+code+' '+message);
					}
					break;
					//exito al obtener los puntos
					case 0: {
						kony.print('MVP.ModelPunto >>> asyncCallbackPuntos >>> error 0.1 >>> resultResponse.length >>> '+resultResponse.length);
						loadListPuntos(resultResponse);
						kony.print('MVP.ModelPunto >>> asyncCallbackPuntos >>> error 0.2 >>> loadListPuntos.size >>> '+_puntosData.size());
						loadPuntosMap(_tipoPunto);
						kony.print('MVP.ModelPunto >>> asyncCallbackPuntos >>> error 0.3 >>> locationData.size >>> '+_locationData.size());

						_info.code = 0;
						_info.message.mensaje = message;
            _info.tipoPunto = _tipoPunto;
						_info.locationData = _locationData.values();
					}
					break;
					//error desconocido
					default: {
						_info.code = code;
						_info.message.titulo = 'Puntos de Atención';
            _info.message.mensaje = message+'.';
						kony.print('MVP.ModelPunto >>> asyncCallbackPuntos >>> error default >>> '+code+' '+message);
					}
					break;
				}
				this.emit('changedWsGetPuntos', _info);
			} else {
        _info.code = results.opstatus;
				_info.message = getErrorConexion(results);
        kony.print('MVP.ModelPunto >>> asyncCallbackPuntos >>> opstatus <> 0 >>> '+_info.code+' '+_info.message);
				this.emit('changedWsGetPuntos', _info);
  		}
		} else {
  		if (status==300) {
				_info.code = results.opstatus;
				_info.message = getErrorConexion(results);
        kony.print('MVP.ModelPunto >>> asyncCallbackPuntos >>> status 300 >>> '+_info.code+' '+JSON.stringify(_info.message));
				//this.emit('changedWsGetPuntos', _info);
  		}
		}
	}

  function asyncCallbackPuntoPropiedades(status, results) {
    if (status==400) {
			kony.print('MVP.ModelPunto >>> asyncCallbackPuntoPropiedades >>> status 400 >>> '+JSON.stringify(results));//results.opstatus+' '+results.errmsg);
			if (results.opstatus == '0') {
				var resultResponse = results.mtdObtenerPuntosResponse;
				var code = resultResponse[0].intCodError;
				var message = resultResponse[0].strMensaje;

				switch (Number(code)) {
					//sesion expirada
					case -1: {
						_info.code = -1;
            results.opstatus = -1;
						_info.message = getErrorConexion(results);;
						kony.print('MVP.ModelPunto >>> asyncCallbackPuntoPropiedades >>> error -1 >>> '+code+' '+message);
					}
					break;
					//exito al obtener las propiedades
					case 0: {
						kony.print('MVP.ModelPunto >>> asyncCallbackPuntoPropiedades >>> error 0.1 >>> resultResponse.length >>> '+resultResponse.length);
						loadListPuntosPropiedades(resultResponse);
						kony.print('MVP.ModelPunto >>> asyncCallbackPuntoPropiedades >>> error 0.2 >>> loadListPuntos.size >>> '+_puntoSelected.propiedades.size());

						_info.code = 0;
						_info.message.mensaje = message;
            _info.punto = _puntoSelected;
					}
					break;
					//error desconocido
					default: {
						_info.code = code;
						_info.message.titulo = 'Puntos de Atención';
            _info.message.mensaje = message+'.';
						kony.print('MVP.ModelPunto >>> asyncCallbackPuntoPropiedades >>> error default >>> '+code+' '+message);
					}
					break;
				}
				this.emit('changedWsGetPuntoPropiedades', _info);
			} else {
        _info.code = results.opstatus;
				_info.message = getErrorConexion(results);
        kony.print('MVP.ModelPunto >>> asyncCallbackPuntoPropiedades >>> opstatus <> 0 >>> '+_info.code+' '+_info.message);
				this.emit('changedWsGetPuntoPropiedades', _info);
  		}
		} else {
  		if (status==300) {
				_info.code = results.opstatus;
				_info.message = getErrorConexion(results);
        kony.print('MVP.ModelPunto >>> asyncCallbackPuntos >>> status 300 >>> '+_info.code+' '+JSON.stringify(_info.message));
				//this.emit('changedWsGetPuntos', _info);
  		}
		}
  }

  /**
   * Class Puntos Model
   */
  var ModelPunto = kony.Stapes.subclass({

  	'constructor': function() {
			_info = {};
  		_locationData = new Hashtable();
			_puntosData = new Hashtable();

      this.handleWSGetPuntos = undefined;
      this.handleWSGetPuntoPropiedades = undefined;

      this.init();
  	},

    'init': function() {
      _tipoPunto = 'X';
      _idDpto = 7;
    },

  	'geoDefaultPosition': function() {
      _locationData.clear();
      _puntosData.clear();

  		var location = {
				'lat': -17.7825888889,
				'lon': -63.1806194444,
				'name': '0',
				'desc': '0',
				'image': getImagen('A','A',true, true),
				'showcallout': false
			};
			putLocation(0, location);

			var punto = {
				'idMapa': 0,
				'idPunto': 0,
				'idTipo': 'A',
        'descTipo': '',
				'idSubTipo': 'A',
				'nombre': '0',
				'direccion': '0',
				'latitud': -17.7825888889,
				'longitud': -63.1806194444,
				'idDpto': 0,
				'descDpto': '0',
        'telefono': '0',
        'distancia': 0,
				'propiedades': new Hashtable(),
				'isCentral': true,
				'isVisible': true,
				'isSelected': true
			};
			putPunto(0, punto);

	    if (_info.code === 0)
				_info.locationData = _locationData.values();

	  	kony.print('MVP.ModelPunto >>> geoDefaultPosition >>> '+JSON.stringify(_info));
	  	this.emit('changedDefaultPosition', _info);
  	},

  	'geoPosition': function(timeout, useBestProvider) {
			var positionOptions = {
			  'enableHighAccuracy': true,
			  'timeout': Number(timeout),
			  'maximumAge': -1,
			  'useBestProvider': useBestProvider
			};

      kony.location.getCurrentPosition(
        geoSuccessCallBack.bind(this),
        geoErrorCallBack.bind(this),
        positionOptions
      );
  	},

  	'wsGetPuntos': function(codPersona, pTipoPunto) {
      _tipoPunto = pTipoPunto;
      /*
      //hasta tener conocimientos de websql
      var params = {
        serviceID: 'mtdObtenerPuntos',
        codusuario: encrypt(codPersona)+''
      };
      kony.print("MVP.ModelPunto >>> wsGetPuntos >>> params >>> "+JSON.stringify(params));
      this.handleWSGetPuntos = appmiddlewaresecureinvokerasync(
  			params,
  			asyncCallbackPuntos.bind(this)
			);
      */

      //para simular datos estaticos
      this.handleWSGetPuntos = undefined;
      wsGetPuntosStatic.call(this);
  	},

    'wsGetPuntoPropiedades': function() {
      if (_puntoSelected.propiedades.values().length === 0) {
    		var params = {
    			serviceID: 'mtdObtenerDatosPuntos',
    			lPuntos: encrypt(_puntoSelected.idPunto)+''
    		};
    		kony.print("MVP.ModelPunto >>> wsGetPuntoPropiedades >>> params >>> "+JSON.stringify(params));

    		this.handleWSGetPuntoPropiedades = appmiddlewaresecureinvokerasync(
          params,
          asyncCallbackPuntoPropiedades.bind(this)
        );
      } else {
        _info.code = 0;
        _info.punto = _puntoSelected;
        this.emit('changedWsGetPuntoPropiedades', _info);
      }
    },

    'setFechaProceso': function(fechaProceso) {
      _fechaProceso = fechaProceso;
    },

    'setSelectLocation': function(location) {
			kony.print("MVP.ModelPunto >>> setSelectPunto >>> location >>> "+JSON.stringify(location));
      _puntoSelected = _puntosData.get(Number(location.desc));
      _puntoSelected.isSelected = false;
      putPunto(Number(location.desc), _puntoSelected);

      loadPuntosMap(_tipoPunto);
      _info.locationData = _locationData.values();
      _info.tipoPunto = _tipoPunto;
      this.emit('changedGetLocationData', _info);
    },

    'setSelectPunto': function(location) {
      kony.print("MVP.ModelPunto >>> getSelectLocation >>> location >>> "+JSON.stringify(_puntosData.get(Number(location.desc))));
      _puntoSelected = _puntosData.get(Number(location.desc));
    },

    'getSelectPunto': function() {
      return _puntoSelected;
    },

    'getLocationDataByTipoPunto': function(pTipoPunto) {
      _tipoPunto = pTipoPunto;

      loadPuntosMap(_tipoPunto);
      _info.tipoPunto = _tipoPunto;
      _info.locationData = _locationData.values();

      kony.print('MVP.PuntosModel >>> getLocationDataByTipoPunto >>> tipoPunto >>> '+_tipoPunto+' >>> puntosData.size() >>> '+_puntosData.size());
      kony.print('MVP.PuntosModel >>> getLocationDataByTipoPunto >>> tipoPunto >>> '+_tipoPunto+' >>> locationData.size() >>> '+_locationData.size());

      this.emit('changedGetLocationData', _info);
    },

    'calculateDistance': function() {
      var punto1 = _puntosData.get(0);
      var punto2 = {};

      kony.print('MVP.PuntosModel >>> calculateDistance >>> punto(0) >>> data >>> '+JSON.stringify(punto1));

      for (var i = 1, len = _puntosData.size(); i<len; i++) {
        punto2 = _puntosData.get(i);

        punto2.distancia = distance(Number(punto1.latitud), Number(punto1.longitud), Number(punto2.longitud), Number(punto2.longitud), "K").formatToString(2, ",", ".");
        //kony.print('MVP.PuntosModel >>> calculateDistance >>> '+ i +' >>> idMap >>> '+punto2.idMapa+' >>> '+punto2.idPunto+' >>> '+punto2.nombre+' >>> distance >>> '+punto2.distancia);

        putPunto(i, punto2);
      }
    },

    'getIdDpto': function() {
      kony.print('MVP.PuntosModel >>> _IdDpto >>> '+_idDpto);

      var puntoMen = {};
      var k, len =_puntosData.size();

      for (k = 1; k<len; k++) {
        puntoMen = _puntosData.get(k);
        if (puntoMen.isVisible && puntoMen.isCentral)
          break;
      }

      var puntoMay = {};
      for (var i = k; i<len; i++) {
        puntoMay = _puntosData.get(i);
        if (puntoMay.isVisible && puntoMay.isCentral && puntoMen.distancia > puntoMay.distancia)
          puntoMen = puntoMay;
      }

      _idDpto = puntoMen.idDpto;

      kony.print('MVP.PuntosModel >>> getIdDpto >>> getIdDpto >>> '+puntoMen.idDpto);
      return puntoMen.idDpto;
    },

    'getIdLocationByDpto': function(pDpto, locationData) {
      kony.print('MVP.PuntosModel >>> getIdLocationByDpto >>> _locationData.size() >>> '+_locationData.size()+' >>> location.length >>> '+locationData.length);
      kony.print('MVP.PuntosModel >>> getIdLocationByDpto >>> _puntosData.size() >>> '+_puntosData.size());

      var location = {};
      var punto = {};
      var idLocation;

      idLocation = -1;
      kony.print('MVP.PuntosModel >>> getIdLocationByDpto >>> _locationData >>> '+JSON.stringify(locationData));

      if (_idDpto == pDpto) {
        idLocation = locationData.length-1;
      } else {
        for (var i = 0; i<locationData.length; i++) {
          location = locationData[i];
          //kony.print('MVP.PuntosModel >>> getIdLocationByDpto >>> locationData'+ i +' >>> '+JSON.stringify(location));
          punto = _puntosData.get(Number(location.desc));

          if (location.desc != '0' && punto.isVisible && punto.isCentral && punto.idDpto == pDpto) {
            //kony.print('MVP.PuntosModel >>> getIdLocationByDpto >>> '+ i +' >>> idPunto >>>'+punto.idPunto+' >>> Dpto >>> '+punto.idDpto);
            idLocation = i;
            break;
          }
        }
      }

      _info.idLocation = idLocation;

      this.emit('changedIdLocationByIdDpto', _info);
    }

  });

  return ModelPunto;
})(kony);
