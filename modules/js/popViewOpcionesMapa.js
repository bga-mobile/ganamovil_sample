kony.print('MVP.PopViewOpcionesMapa >>> Loading JS file popViewOpcionesMapa.js');

var PopViewOpcionesMapa = (function (kony,undefined) {
  var _context;
  var popToastOpcionesMapa;

  function segPuntosOnRowClick(seguiWidget, sectionIndex, rowIndex, selectedState) {
    kony.print('MVP.PopViewOpcionesMapa >>> segPuntosOnRowClick >>> sectionIndex >>> '+sectionIndex+' >>> rowIndex >>> '+rowIndex+' >>> selectedState >>> '+selectedState);
    kony.print('MVP.PopViewOpcionesMapa >>> segPuntosOnRowClick >>> selectedRowIndex'+JSON.stringify(popToastOpcionesMapa.segPuntosAtencion.selectedRowIndex));
    kony.print('MVP.PopViewOpcionesMapa >>> segPuntosOnRowClick >>> selectedRowIndices'+JSON.stringify(popToastOpcionesMapa.segPuntosAtencion.selectedRowIndices));

    var info = {};
    info.rowIndex = rowIndex;
    info.selectedState = selectedState;
    info.key = seguiWidget.selectedItems[0].key;

    this.emit('selectedPuntosAtencion', info);
  }

  function btnEstandarOnClick() {
    this.setSelectedVistaMapa(kModeMap.MAP_VIEW_MODE_NORMAL);
  }

  function btnHibridoOnClick() {
    this.setSelectedVistaMapa(kModeMap.MAP_VIEW_MODE_HYBRID);
  }

  function btnSateliteOnClick() {
    this.setSelectedVistaMapa(kModeMap.MAP_VIEW_MODE_SATELLITE);
  }

  function segPuntosBusquedaOnRowClick(seguiWidget, sectionIndex, rowIndex, selectedState) {
    kony.print('MVP.PopViewOpcionesMapa >>> segPuntosBusquedaOnRowClick >>> sectionIndex >>> '+sectionIndex+' >>> rowIndex >>> '+rowIndex+' >>> selectedState >>> '+selectedState);
  	kony.print('MVP.PopViewOpcionesMapa >>> segPuntosBusquedaOnRowClick >>> selectedRowIndex'+JSON.stringify(popToastOpcionesMapa.segPuntosBusqueda.selectedRowIndex));
  	kony.print('MVP.PopViewOpcionesMapa >>> segPuntosBusquedaOnRowClick >>> selectedRowIndices'+JSON.stringify(popToastOpcionesMapa.segPuntosBusqueda.selectedRowIndices));

    var info = {};
    info.rowIndex = rowIndex;
    info.selectedState = selectedState;
    info.key = seguiWidget.selectedItems[0].key;

    this.emit('selectedFiltroBusqueda', info);
  }

  var PopViewOpcionesMapa = kony.Stapes.subclass({
    'constructor': function(pPopToastOpcionesMapa, pContext) {
      popToastOpcionesMapa = pPopToastOpcionesMapa;
      _context = pContext;

      this.init();
      this.bindUIEventHandlers();
    },

    'init': function() {
      popToastOpcionesMapa.setContext({"widget":_context,"anchor":"bottom","sizetoanchorwidth":true});
    	popToastOpcionesMapa.transparencyBehindThePopup = 100;

      //PUNTOS_ATENCION
      popToastOpcionesMapa.hbxPuntosAtencion.isVisible = false;
      popToastOpcionesMapa.segPuntosAtencion.selectionBehavior = constants.SEGUI_SINGLE_SELECT_BEHAVIOR;
      popToastOpcionesMapa.segPuntosAtencion.selectionBehaviorConfig = {
        imageIdentifier: 'imgSelect',
        selectedStateImage: 'radio_checked.png',
        unselectedStateImage: 'radio_unchecked.png'
      };
      popToastOpcionesMapa.segPuntosAtencion.widgetDataMap = {
        imgIcon: 'imgIcon',
        lblDescripcion: 'descripcion',
        imgSelect:'imgSelected'
      };

      //VISTA_MAPA
    	popToastOpcionesMapa.hbxVistaMapa.isVisible = false;
    	//#ifdef android
    		popToastOpcionesMapa.btnHibrido.isVisible = false;
    	//#endif

      popToastOpcionesMapa.btnEstandar.skin = "slButtonBorder2T100Grey700CNormal110";
      popToastOpcionesMapa.btnEstandar.focusSkin = "slButtonBorder2T100Grey700CNormal110";
      popToastOpcionesMapa.btnHibrido.skin = "slButtonBorder2Grey700CNormal110";
      popToastOpcionesMapa.btnHibrido.focusSkin = "slButtonBorder2Grey700CNormal110";
      popToastOpcionesMapa.btnSatelite.skin = "slButtonBorder2Grey700CNormal110";
      popToastOpcionesMapa.btnSatelite.focusSkin = "slButtonBorder2Grey700CNormal110";

      //FILTRO_BUSQUEDA
    	popToastOpcionesMapa.hbxBusquedaPuntos.isVisible = false;
      popToastOpcionesMapa.segPuntosBusqueda.selectionBehavior = constants.SEGUI_SINGLE_SELECT_BEHAVIOR;
      popToastOpcionesMapa.segPuntosBusqueda.selectionBehaviorConfig = {
        imageIdentifier: 'imgSelect',
        selectedStateImage: 'radio_checked.png',
        unselectedStateImage: 'radio_unchecked.png'
      };
      popToastOpcionesMapa.segPuntosBusqueda.widgetDataMap = {
        lblTipo: 'departamento',
        imgSelect: 'imgSelected'
      };
    },

    /**
	  * Event Handlers for binding this view
	  */
	  'bindUIEventHandlers' : function() {
      //PUNTOS_ATENCION
      popToastOpcionesMapa.segPuntosAtencion.onRowClick = segPuntosOnRowClick.bind(this);
      //VISTA_MAPA
      popToastOpcionesMapa.btnEstandar.onClick = btnEstandarOnClick.bind(this);
      popToastOpcionesMapa.btnHibrido.onClick = btnHibridoOnClick.bind(this);
      popToastOpcionesMapa.btnSatelite.onClick = btnSateliteOnClick.bind(this);
      //FILTRO_BUSQUEDA
      popToastOpcionesMapa.segPuntosBusqueda.onRowClick = segPuntosBusquedaOnRowClick.bind(this);
    },

    'show': function () {
      popToastOpcionesMapa.show();
    },

    'dismiss': function () {
      popToastOpcionesMapa.dismiss();
    },

    'refreshMenuPuntosAtencion': function(data) {
			popToastOpcionesMapa.segPuntosAtencion.setData(data);
    },

    'refreshMenuFiltroBusqueda': function(data) {
			popToastOpcionesMapa.segPuntosBusqueda.setData(data);
    },

    'refreshMenuOpciones': function(opcion) {
      popToastOpcionesMapa.hbxPuntosAtencion.isVisible = false;
      popToastOpcionesMapa.hbxVistaMapa.isVisible = false;
      popToastOpcionesMapa.hbxBusquedaPuntos.isVisible = false;

      switch (opcion) {
        case kOpcionMenuMapa.PUNTOS_ATENCION:
          popToastOpcionesMapa.hbxPuntosAtencion.isVisible = true;
        break;
        case kOpcionMenuMapa.VISTA_MAPA:
          popToastOpcionesMapa.hbxVistaMapa.isVisible = true;
        break;
        case kOpcionMenuMapa.FILTRO_BUSQUEDA:
          popToastOpcionesMapa.hbxBusquedaPuntos.isVisible = true;
        break;
      }
    },

    'setSelectedPuntosAtencion': function(key) {
      var segData = popToastOpcionesMapa.segPuntosAtencion.data;
      var i, len;
      for (i = 0,  len = segData.length; i < len; i++) {
        if (segData[i].key === key) {
          popToastOpcionesMapa.segPuntosAtencion.selectedIndex = [0,i];
          popToastOpcionesMapa.segPuntosAtencion.selectedRowIndices = [[0,[i]]];
          break;
        }
      }
    },

    'setSelectedVistaMapa': function(modeMap) {
      switch (modeMap) {
        case kModeMap.MAP_VIEW_MODE_NORMAL: {
          popToastOpcionesMapa.btnEstandar.skin = "slButtonBorder2T100Grey700CNormal110";
          popToastOpcionesMapa.btnEstandar.focusSkin = "slButtonBorder2T100Grey700CNormal110";
          popToastOpcionesMapa.btnHibrido.skin = "slButtonBorder2Grey700CNormal110";
          popToastOpcionesMapa.btnHibrido.focusSkin = "slButtonBorder2Grey700CNormal110";
          popToastOpcionesMapa.btnSatelite.skin = "slButtonBorder2Grey700CNormal110";
          popToastOpcionesMapa.btnSatelite.focusSkin = "slButtonBorder2Grey700CNormal110";
        }
        break;
        case kModeMap.MAP_VIEW_MODE_HYBRID: {
          popToastOpcionesMapa.btnEstandar.skin = "slButtonBorder2Grey700CNormal110";
          popToastOpcionesMapa.btnEstandar.focusSkin = "slButtonBorder2Grey700CNormal110";
          popToastOpcionesMapa.btnHibrido.skin = "slButtonBorder2T100Grey700CNormal110";
          popToastOpcionesMapa.btnHibrido.focusSkin = "slButtonBorder2T100Grey700CNormal110";
          popToastOpcionesMapa.btnSatelite.skin = "slButtonBorder2Grey700CNormal110";
          popToastOpcionesMapa.btnSatelite.focusSkin = "slButtonBorder2Grey700CNormal110";
        }
        break;
        case kModeMap.MAP_VIEW_MODE_SATELLITE: {
          popToastOpcionesMapa.btnEstandar.skin = "slButtonBorder2Grey700CNormal110";
          popToastOpcionesMapa.btnEstandar.focusSkin = "slButtonBorder2Grey700CNormal110";
          popToastOpcionesMapa.btnHibrido.skin = "slButtonBorder2Grey700CNormal110";
          popToastOpcionesMapa.btnHibrido.focusSkin = "slButtonBorder2Grey700CNormal110";
          popToastOpcionesMapa.btnSatelite.skin = "slButtonBorder2T100Grey700CNormal110";
          popToastOpcionesMapa.btnSatelite.focusSkin = "slButtonBorder2T100Grey700CNormal110";
        }
        break;
      }
      this.emit('changedModeMap', modeMap);
    },

    'setIndexSelectedFiltroBusqueda': function(key) {
      var segData = popToastOpcionesMapa.segPuntosBusqueda.data;
      var i, len;
      for (i = 0,  len = segData.length; i < len; i++) {
        if (segData[i].key === key) {
          popToastOpcionesMapa.segPuntosBusqueda.selectedIndex = [0,i];
          popToastOpcionesMapa.segPuntosBusqueda.selectedRowIndices = [[0,[i]]];
          break;
        }
      }
    }

  });

  return PopViewOpcionesMapa;
})(kony);
