Number.prototype.formatToString = function(decPlaces, thouSeparator, decSeparator) {
    var n = this,
        decPlaces = isNaN(decPlaces = Math.abs(decPlaces)) ? 2 : decPlaces,
        decSeparator = decSeparator == undefined ? "." : decSeparator,
        thouSeparator = thouSeparator == undefined ? "," : thouSeparator,
        sign = n < 0 ? "-" : "",
        i = parseInt(n = Math.abs(+n || 0).toFixed(decPlaces)) + "",
        j = (j = i.length) > 3 ? j % 3 : 0;
    return sign + (j ? i.substr(0, j) + thouSeparator : "") + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + thouSeparator) + (decPlaces ? decSeparator + Math.abs(n - i).toFixed(decPlaces).slice(2) : "");
}

function formatNumberString(number, decPlaces, thouSeparator, decSeparator) {
    var n = number,
        decPlaces = isNaN(decPlaces = Math.abs(decPlaces)) ? 2 : decPlaces,
        decSeparator = decSeparator == undefined ? "." : decSeparator,
        thouSeparator = thouSeparator == undefined ? "," : thouSeparator,
        sign = n < 0 ? "-" : "",
        i = parseInt(n = Math.abs(+n || 0).toFixed(decPlaces)) + "",
        j = (j = i.length) > 3 ? j % 3 : 0;
    return sign + (j ? i.substr(0, j) + thouSeparator : "") + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + thouSeparator) + (decPlaces ? decSeparator + Math.abs(n - i).toFixed(decPlaces).slice(2) : "");
}

function removeZerosVA(ipString){
	var tempString = "";
	var fieldValidateVA = "";
	tempString = ipString;
	tempString = kony.string.reverse(tempString);
	var strLength = kony.string.len(tempString);
	if(((tempString).charAt (strLength-1) == "0")){
		if(((tempString).charAt (strLength - 2) == ".")){
			fieldValidateVA = kony.string.reverse(tempString);
		}
		//tempString = kony.string.sub(tempString, 1, strLength - 1);
		tempString = tempString.substring(0,strLength-1);
		fieldValidateVA = removeZerosVA(kony.string.reverse(tempString));
	}else{
		fieldValidateVA = kony.string.reverse(tempString);
	}
	return fieldValidateVA;
}
