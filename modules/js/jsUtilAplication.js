var kPregunta = {
	NOTHING: 0,
	ENROLAMIENTO: 1,
	DEVICE_BACK: 2,
	LOCATION_SERVICE: 3,
	EXIT_APP: 4
}
var kFrmMobile = {
	DEFAULT: '0',
	LOGIN: '1',
	DASHBOARD: '2',
	MAPA: '3'
}
var kOpcionMenuMapa = {
	PUNTOS_ATENCION: '1',
	VISTA_MAPA: '2',
	POSICION_ACTUAL: '3',
	FILTRO_BUSQUEDA: '4'
};

var kModeMap = {
	MAP_VIEW_MODE_NORMAL: constants.MAP_VIEW_MODE_NORMAL,
	MAP_VIEW_MODE_HYBRID: constants.MAP_VIEW_MODE_HYBRID,
	MAP_VIEW_MODE_SATELLITE: constants.MAP_VIEW_MODE_SATELLITE
};

var PopPregunta = {
	FRM_LOGIN_ENROLAMIENTO: 1,
	FRM_DASHBOARD_DEVICE_BACK: 2,
	FRM_LOCATION_SERVICE: 3,
	APP_EXIT: 4
};

var Comprobante = {
	TRANSFER: "TRF",
	PAGO: "PGS",
	TARJETA: "TRJ",
	PRESTAMO: "PTM"
};

var TipoCtaDestino = {
	CTA_PROPIAS : "false",
	CTA_TERCEROS : "true",
	CTA_OTROS_BANCOS : "ws"
};

var TipoDoc = {
	YES_NIT : "S",
	NOT_NIT : "N",
	FACTURA : ""
};

var FormaPagoServicio = {
	NINGUNO: "N/A",
	UNO: "LU",
	VARIOS: "LV",
	MONTO: "M",
	MONTO_ITEM: "MI",
	CANTIDAD_MULT: "CM",
	CANTIDAD_ITEM: "CI",
	SHOW : "LST",
	CHECK : "CHK"
};

var Acento = {
    a : "\u00e1",
    e : "\u00e9",
    i : "\u00ed",
    o : "\u00f3",
    u : "\u00fa",
    A : "\u00c1",
    E : "\u00c9",
    I : "\u00cd",
    O : "\u00d3",
    U : "\u00da",
    enhe : "\u00f1",
    ENHE : "\u00d1"
};

var AppConfig = {
    APP_NAME : "GanaM" + Acento.o + "vil",
    TIME_OUT : 3000,
    PASSPRHASE : "KonyLabs70866829"
};

var ErrorCodigo = {
	1000 : "1000",
	1011 : "1011",
	1012 : "1012",
	1013 : "1013",
	1014 : "1014",
	1015 : "1015",
	1016 : "1016",
	1200 : "1200"
};

var MensajePop = {
    CERRAR_SESION : "¿Deseas cerrar sesi" + Acento.o + "n?",
    NOTA_TRANSACCION : "NOTA: Antes de reintentar verifica en tu extracto si la transacci"+Acento.o+"n ha sido efectuada."
};


var ErrorConexion = {
    //100 - network call initiated successfully. The resultset is not available, and it is nil.
    //100 : llamadas de la red inició con éxito. El conjunto de resultados no está disponible, y es nula.
    //200 - network is in progress (when you start receiving the first byte). The resultset is not available, and it is nil.
    //200 - la red está en curso (cuando comienza a recibir el primer byte). El conjunto de resultados no está disponible, y es nula.
    //300 - network call canceled. The resultset is not available and it is nil.
    //300 - llamadas de la red cancelada. El conjunto de resultados no está disponible y es nula.
    '-1': {titulo: "Sesi" + Acento.o + "n Expirada", mensaje: "Ha sido desconectado por exceder el tiempo l" + Acento.i + "mite de inactividad."},
    //1000 - Unknown error while connecting (if the platform cannot differentiate between network errors, the platform reports error code 1000 by default).
    //1000 : {titulo:"Error de Conexi"+Acento.o+"n", mensaje:"El dispositivo no tiene Wi-Fi o acceso a Internet. Intenta nuevamente despu"+Acento.e+"s de establecer la conectividad."},
    '1000' : {titulo: "Error de Comunicaci" + Acento.o + "n", mensaje: "Error desconocido durante la conexi" + Acento.o + "n al servidor. Por favor intenta mas tarde."},
    '1001' : {titulo: "Error de Comunicaci" + Acento.o + "n", mensaje: "Error desconocido durante la conexi" + Acento.o + "n al servidor. Por favor intenta mas tarde."},
    //1011 - Device has no Wi-Fi or mobile connectivity. Try the operation after establishing connectivity.
    '1011' : {titulo: "Error de Conexi" + Acento.o + "n", mensaje: "El dispositivo no tiene Wi-Fi o acceso a Internet. Intenta nuevamente despu" + Acento.e + "s de establecer la conectividad."},
    //1012 - Request failed.
    '1012' : {titulo: "Error de Comunicaci" + Acento.o + "n", mensaje: "La solicitud al servidor ha fallado. Por favor intenta nuevamente."},
    //1013 - Middleware returned invalid JSON string.
    '1013' : {titulo: "Error de Comunicaci" + Acento.o + "n", mensaje: "La respuesta del servidor ha fallado. Por favor intenta nuevamente."},
    //1014 - Request timed out.
    '1014' : {titulo: "Error de Comunicaci" + Acento.o + "n", mensaje: "El tiempo de espera de respuesta ha excedido. Por favor intenta nuevamente."},
    //1015 - Cannot find host.
    '1015' : {titulo: "Error de Comunicaci" + Acento.o + "n", mensaje: "No es posible encontrar el Servidor. Por favor intenta mas tarde."},
    //1016 - Cannot connect to host.
    '1016' : {titulo: "Error de Comunicaci" + Acento.o + "n", mensaje: "No es posible conectarse al Servidor. Por favor intenta mas tarde."},
    //1200 - SSL - Certificate related error codes.
    '1200' : {titulo: "Error de Comunicaci" + Acento.o + "n", mensaje: "No es posible validar el Certificado de Seguridad. Por favor intenta nuevamente."},
    //9001
    '9001' : {titulo: "Error de Comunicaci" + Acento.o + "n", mensaje: "El tiempo de espera de respuesta ha excedido. Por favor intenta nuevamente.."},
    //8009
    '8009' : {titulo: "Error de Conexi" + Acento.o + "n", mensaje: "Error desconocido durante la conexi" + Acento.o + "n al servidor. Por favor intenta nuevamente."}
};

/**
 * Cancela la llamada a un WS
 */
function cancelService(connHandle) {
	try {
        kony.net.cancel(connHandle);
	} catch (e) {
        // todo: handle exception
        alert(e);
	}
}

/**
 * Cierra la aplicacion y retorna a la pantalla de Login
 */
function exitApplication() {
	initFrmLogin();
	frmLogin.show();
	/*
	try{
		kony.application.exit();
	} catch(Error) {
   		showPopMensaje("Error de Aplicacion","Excepcion mientras se intenta salir de la aplicacion : "+Error);
   	}
   	*/
}

/**
 * Retorna el estado de la conexion a la red
 * @returns {Boolean}
 */
function isNetworkAvailable() {
	if (!kony.net.isNetworkAvailable(constants.NETWORK_TYPE_ANY) ) {
		var results = {intCodError:"1011"};
		showMensajeError(results);
	}
	return kony.net.isNetworkAvailable(constants.NETWORK_TYPE_ANY);
}

/**
 * carga una pantalla de loading
 */
function showLoadingScreen(skin, msj) {
	if ( kony.os.deviceInfo().name == "iPhone" || kony.os.deviceInfo().name == "iPad" ) {
	   kony.application.showLoadingScreen(
			skin,
			msj,
			constants.LOADING_SCREEN_POSITION_FULL_SCREEN,
			true,
			true,
			{
				shouldShowLabelInBottom:true,
				separatorHeight:30
			}
		);
	} else {
		kony.application.showLoadingScreen(
			skin,
			msj,
			constants.LOADING_SCREEN_POSITION_FULL_SCREEN,
			true,
			true,
			null
		);
	}
}

function showPreviousForm() {
	//Get the Previous form
	var previousForm = kony.application.getPreviousForm();
	previousForm.show();
}

/**
 * Muestra errores comunes de la Aplicacion
 * @param {Array} results
 */
function showMensajeError(results, isDashboard){
	//valor de parametro por defecto
	isDashboard || (isDashboard = false);

	var codError = Number(""+results.opstatus);
	if (isNaN(codError))
		codError = 1011;

	if (isDashboard) {
		initFrmDashboard();
		if (ErrorConexion[""+codError] != undefined)
			showPopMensaje(ErrorConexion[""+codError].titulo, ErrorConexion[codError].mensaje+". \n\n"+MensajePop.NOTA_TRANSACCION, ErrorMensaje.MENSAJE);
		else
			showPopMensaje("Error...", "Error: "+results.opstatus+" "+results.errcode+" "+results.errmsg+". \n\n"+MensajePop.NOTA_TRANSACCION, ErrorMensaje.MENSAJE);
	} else {
		switch (codError) {
			case -1:
				showPopMensaje(ErrorConexion["-1"].titulo, ErrorConexion["-1"].mensaje, ErrorMensaje.SESION_EXPIRADA);
			break;
			case 0:
				showPopMensaje("Advertencia", "Error: "+results.intCodError+" "+results.strMensaje+".", ErrorMensaje.MENSAJE);
			break;
			case 1000:
				showPopMensaje(ErrorConexion["1000"].titulo, ErrorConexion["1000"].mensaje, ErrorMensaje.MENSAJE);
			break;
			case 1001:
				showPopMensaje(ErrorConexion["1001"].titulo, ErrorConexion["1001"].mensaje, ErrorMensaje.MENSAJE);
			break;
			case 1011:
				showPopMensaje(ErrorConexion["1011"].titulo, ErrorConexion["1011"].mensaje, ErrorMensaje.MENSAJE);
			break;
			case 1012:
				showPopMensaje(ErrorConexion["1012"].titulo, ErrorConexion["1012"].mensaje, ErrorMensaje.MENSAJE);
			break;
			case 1013:
				showPopMensaje(ErrorConexion["1013"].titulo, ErrorConexion["1013"].mensaje, ErrorMensaje.MENSAJE);
			break;
			case 1014:
				showPopMensaje(ErrorConexion["1014"].titulo, ErrorConexion["1014"].mensaje, ErrorMensaje.MENSAJE);
			break;
			case 1015:
				showPopMensaje(ErrorConexion["1015"].titulo, ErrorConexion["1015"].mensaje, ErrorMensaje.MENSAJE);
			break;
			case 1016:
				showPopMensaje(ErrorConexion["1016"].titulo, ErrorConexion["1016"].mensaje, ErrorMensaje.MENSAJE);
			break;
			case 1200:
				showPopMensaje(ErrorConexion["1200"].titulo, ErrorConexion["1200"].mensaje, ErrorMensaje.MENSAJE);
			break;
			case 9001:
				showPopMensaje(ErrorConexion["9001"].titulo, ErrorConexion["9001"].mensaje, ErrorMensaje.MENSAJE);
			break;
			case 8009:
				showPopMensaje(ErrorConexion["8009"].titulo, ErrorConexion["8009"].mensaje, ErrorMensaje.MENSAJE);
			break;
			default:
				showPopMensaje("Error...", "Error: "+results.opstatus+" "+results.errcode+" "+results.errmsg+".", ErrorMensaje.MENSAJE);
			break;
		}
	}
}


/**
 * Obtener error comunes de la Aplicacion
 * @param {Array} results
 * @param {boolean} isTransaccion
 */
function getErrorConexion(results, isTransaccion){
	//valor de parametro por defecto
	isTransaccion || (isTransaccion = false);

	var errorConexion = {};
	var codError = Number(""+results.opstatus);
	if (isNaN(codError))
		codError = 1011;

	if (ErrorConexion.hasOwnProperty(codError+'')) {
		errorConexion.titulo = ErrorConexion[codError+''].titulo;
		errorConexion.mensaje = ErrorConexion[codError+''].mensaje + (isTransaccion? '\n\n' + MensajePop.NOTA_TRANSACCION : '');
	}	else {
		errorConexion.titulo = ErrorConexion[codError+''].titulo;
		errorConexion.mensaje = 'Error: ' + results.opstatus + ' ' +results.errcode + ' ' + results.errmsg + (isTransaccion? '. \n\n' + MensajePop.NOTA_TRANSACCION : '.');
	}

	return errorConexion;
}
